<!DOCTYPE html>
<html lang="en">


<?php include('includes/frontend/modules/head.php')?>


  <body class="top" id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
    <!-- Preloader-->
    <div id="preloader">
      <div id="status"></div>
    </div>
    <!-- Navigation-->
    <?php include('includes/frontend/modules/nav_wo_slider.php')?>

    <!-- Header-->
    <header class="intro introhalf" data-background="img/header/1.jpg">
      <div class="intro-body">
        <h1>Our Suites</h1>
        <!--<h4>Our happy clients</h4>-->
      </div>
    </header>
    <!-- News Block-->
    <section class="section-small" id="news">
      <div class="container">
        <div class="row text-center">
          <div class="col-md-10 col-md-offset-1">
            <h3>Amazing oceanfront Suites</h3>
            <!--<p class="text-justify">and we like to think they love us too! Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque efficitur nulla orci, in sollicitudin mauris viverra id. Donec est lectus, eleifend vitae rutrum eu, porta sed nisl. Nunc vehicula enim eget risus faucibus, id interdum nisi commodo. Nunc eget massa eget ipsum.</p>-->
          </div>
        </div>
        <div class="row grid-pad">
          <div class="col-sm-6">
            <a href="suite-1">
              <img class="img-responsive center-block" src="img/rooms/1/1.jpg" alt="">
              <h5>SUITE 1</h5></a>
            <!--<p>Lorem ipsum dolor sit amet, consectetur elit. Nulla convallis pulvinar vestibulum. Donec eleifend, sem sed dictum mattis, turpis purus placerat</p><a class="btn btn-gray btn-xs" href="news-single.html">Read more</a>-->
          </div>
          <div class="col-sm-6">
            <a href="suite-2">
              <img class="img-responsive center-block" src="img/rooms/2/1.jpg" alt="">
              <h5>SUITE 2</h5></a>
            <!--<p>Lorem ipsum dolor sit amet, consectetur elit. Nulla convallis pulvinar vestibulum. Donec eleifend, sem sed dictum mattis, turpis purus placerat</p><a class="btn btn-gray btn-xs" href="news-single.html">Read more</a>-->
          </div>
          <div class="col-sm-6">
            <a href="suite-3">
              <img class="img-responsive center-block" src="img/rooms/3/1.jpg" alt="">
              <h5>SUITE 3</h5></a>
            <!--<p>Lorem ipsum dolor sit amet, consectetur elit. Nulla convallis pulvinar vestibulum. Donec eleifend, sem sed dictum mattis, turpis purus placerat</p><a class="btn btn-gray btn-xs" href="news-single.html">Read more</a>-->
          </div>
          <div class="col-sm-6">
            <a href="suite-4">
              <img class="img-responsive center-block" src="img/rooms/4/1.jpg" alt="">
              <h5>SUITE 4</h5></a>
            <!--<p>Lorem ipsum dolor sit amet, consectetur elit. Nulla convallis pulvinar vestibulum. Donec eleifend, sem sed dictum mattis, turpis purus placerat</p><a class="btn btn-gray btn-xs" href="news-single.html">Read more</a>-->
          </div>
          <div class="col-sm-6">
            <a href="suite-5">
              <img class="img-responsive center-block" src="img/rooms/5/1.jpg" alt="">
              <h5>SUITE 5</h5></a>
            <!--<p>Lorem ipsum dolor sit amet, consectetur elit. Nulla convallis pulvinar vestibulum. Donec eleifend, sem sed dictum mattis, turpis purus placerat</p><a class="btn btn-gray btn-xs" href="news-single.html">Read more</a>-->
          </div>
          <div class="col-sm-6">
            <a href="suite-6">
              <img class="img-responsive center-block" src="img/rooms/6/1.jpg" alt="">
              <h5>SUITE 6</h5></a>
            <!--<p>Lorem ipsum dolor sit amet, consectetur elit. Nulla convallis pulvinar vestibulum. Donec eleifend, sem sed dictum mattis, turpis purus placerat</p><a class="btn btn-gray btn-xs" href="news-single.html">Read more</a>-->
          </div>
          <div class="col-sm-6">
            <a href="suite-7">
              <img class="img-responsive center-block" src="img/rooms/7/1.jpg" alt="">
              <h5>SUITE 7</h5></a>
            <!--<p>Lorem ipsum dolor sit amet, consectetur elit. Nulla convallis pulvinar vestibulum. Donec eleifend, sem sed dictum mattis, turpis purus placerat</p><a class="btn btn-gray btn-xs" href="news-single.html">Read more</a>-->
          </div>
          <div class="col-sm-6">
            <a href="penthouse">
              <img class="img-responsive center-block" src="img/rooms/PH/1.jpg" alt="">
              <h5>PENTHOUSE</h5></a>
            <!--<p>Lorem ipsum dolor sit amet, consectetur elit. Nulla convallis pulvinar vestibulum. Donec eleifend, sem sed dictum mattis, turpis purus placerat</p><a class="btn btn-gray btn-xs" href="news-single.html">Read more</a>-->
          </div>
        </div>
      </div>
    </section>
    <!-- Pagination-->
    <!--<div class="section section-small bg-white">-->
      <!--<div class="container">-->
        <!--<div class="row">-->
          <!--<div class="col-lg-12 text-center">-->
            <!--<nav>-->
              <!--<ul class="pagination">-->
                <!--<li><a href="news2.html#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>-->
                <!--<li class="active"><a href="news2.html#">1<span class="sr-only">(current)</span></a></li>-->
                <!--<li><a href="news2.html#">2</a></li>-->
                <!--<li><a href="news2.html#">&middot;&middot;&middot;</a><a href="news2.html#">38</a></li>-->
                <!--<li><a href="news2.html#" aria-label="Previous"><span aria-hidden="true">&raquo;</span></a></li>-->
              <!--</ul>-->
            <!--</nav>-->
          <!--</div>-->
        <!--</div>-->
      <!--</div>-->
    <!--</div>-->


    <!-- Subscribe Section-->
    <?php include('includes/frontend/modules/suscribe.php')?>

    <!-- Footer Section-->
    <?php include('includes/frontend/modules/footer.php')?>

    <!-- jQuery-->
    <script src="js/jquery-1.12.4.min.js"></script>
    <!-- Bootstrap Core JavaScript-->
    <script src="js/bootstrap.min.js"></script>
    <!-- Plugin JavaScript-->
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/device.min.js"></script>
    <script src="js/form.min.js"></script>
    <script src="js/jquery.placeholder.min.js"></script>
    <script src="js/jquery.shuffle.min.js"></script>
    <script src="js/jquery.parallax.min.js"></script>
    <script src="js/jquery.circle-progress.min.js"></script>
    <script src="js/jquery.swipebox.min.js"></script>
    <script src="js/smoothscroll.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/jquery.smartmenus.js"></script>
    <!-- Custom Theme JavaScript-->
    <script src="js/main.js"></script>
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
  </body>
</html>
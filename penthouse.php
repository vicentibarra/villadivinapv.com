<!DOCTYPE html>
<html lang="en">
<?php
$title = 'Penthouse';
include('includes/frontend/modules/head.php')
?>
<body class="top" id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<!-- Preloader-->
<div id="preloader">
    <div id="status"></div>
</div>
<!-- Navigation-->
<?php include('includes/frontend/modules/nav.php')?>

<!-- Header -->
<header class="intro carousel carousel-big slide carousel-fade" id="Carousel-intro" data-ride="carousel">
    <!-- Indicators-->
    <ol class="carousel-indicators">
        <li class="active" data-target="#Carousel-intro" data-slide-to="0"></li>
        <li data-target="#Carousel-intro" data-slide-to="1"></li>
        <li data-target="#Carousel-intro" data-slide-to="2"></li>
        <li data-target="#Carousel-intro" data-slide-to="3"></li>
        <li data-target="#Carousel-intro" data-slide-to="4"></li>
        <li data-target="#Carousel-intro" data-slide-to="5"></li>
        <li data-target="#Carousel-intro" data-slide-to="6"></li>
        <li data-target="#Carousel-intro" data-slide-to="7"></li>
    </ol>
    <div class="carousel-inner">
        <div class="item active">
            <div class="fill" style="background-image:url('img/rooms/PH/1.jpg');">
                <div class="intro-body">
                    <h1 class="wow">PENTHOUSE</h1>
                    <a class="page-scroll" href="#details">
                        <span class="mouse">
                            <span>
                                <i class="icon ion-ios-arrow-down"></i>
                            </span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="fill" style="background-image:url('img/rooms/PH/2.jpg');">
                <div class="intro-body">
                    <h1 class="wow">PENTHOUSE</h1>
                    <a class="page-scroll" href="#details">
                        <span class="mouse">
                            <span>
                                <i class="icon ion-ios-arrow-down"></i>
                            </span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="fill" style="background-image:url('img/rooms/PH/3.jpg');">
                <div class="intro-body">
                    <h1 class="wow">PENTHOUSE</h1>
                    <a class="page-scroll" href="#details">
                        <span class="mouse">
                            <span>
                                <i class="icon ion-ios-arrow-down"></i>
                            </span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="fill" style="background-image:url('img/rooms/PH/4.jpg');">
                <div class="intro-body">
                    <h1 class="wow">PENTHOUSE</h1>
                    <a class="page-scroll" href="#details">
                        <span class="mouse">
                            <span>
                                <i class="icon ion-ios-arrow-down"></i>
                            </span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="fill" style="background-image:url('img/rooms/PH/5.jpg');">
                <div class="intro-body">
                    <h1 class="wow">PENTHOUSE</h1>
                    <a class="page-scroll" href="#details">
                        <span class="mouse">
                            <span>
                                <i class="icon ion-ios-arrow-down"></i>
                            </span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="fill" style="background-image:url('img/rooms/PH/6.jpg');">
                <div class="intro-body">
                    <h1 class="wow">PENTHOUSE</h1>
                    <a class="page-scroll" href="#details">
                        <span class="mouse">
                            <span>
                                <i class="icon ion-ios-arrow-down"></i>
                            </span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="fill" style="background-image:url('img/rooms/PH/7.jpg');">
                <div class="intro-body">
                    <h1 class="wow">PENTHOUSE</h1>
                    <a class="page-scroll" href="#details">
                        <span class="mouse">
                            <span>
                                <i class="icon ion-ios-arrow-down"></i>
                            </span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="fill" style="background-image:url('img/rooms/PH/8.jpg');">
                <div class="intro-body">
                    <h1 class="wow">PENTHOUSE</h1>
                    <a class="page-scroll" href="#details">
                        <span class="mouse">
                            <span>
                                <i class="icon ion-ios-arrow-down"></i>
                            </span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- Controls-->
    <a class="left carousel-control" href="#Carousel-intro" data-slide="prev">
        <span class="icon-prev"></span>
    </a>
    <a class="right carousel-control" href="#Carousel-intro"data-slide="next">
        <span class="icon-next"></span>
    </a>
</header>


<section id="details">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <!--<h3></h3>-->
                <p class="text-justify">The penthouse suite represents the very best of Villa Divina, offering guests privacy and intimacy. Located on the top floor of the villa, Penthouse guests have access to their own private infinity pool, lounge and living areas, kitchen, and a balcony with the villa’s best sea views. It’s the perfect spot to relax and go on full vacation mode with friends or loved ones.</p>
                <p class="text-justify">The two rooms in the penthouse boast a gorgeous, breezy layout that’ll immediately put you into relaxation. Each room features one king-size bed, an ensuite bathroom, and tasteful furnishings and decor. A deck set can also be found on the balcony.</p>
                <p class="text-justify">The penthouse can be rented either individually, or as part of the larger villa. In either case, penthouse guests have their own private villa access. Like all other guests, they have free access to the villa’s gym, kitchen and the main heated pool. Internet is provided free of charge. The room also has SKY satellite TV and Netflix.</p>

            </div>
        </div>
        <div class="row text-gray-vd">
            <div class="col-lg-offset-2 col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/aire.png" alt="">
                </div>
                <h4>A/C</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/TV%20satelital.png" alt="">
                </div>
                <h4>Satellite TV</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/internet.png" alt="">
                </div>
                <h4>WIFI</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/NETFLIX.png" alt="">
                </div>
                <h4>NETFLIX</h4>

            </div>
        </div>
        <div class="row text-gray-vd">
            <div class="col-lg-offset-2 col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/Caja%20fuerte.png" alt="">
                </div>
                <h4>Security Box</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/Cafetera.png" alt="">
                </div>
                <h4>Coffee Maker</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/Plancha.png" alt="">
                </div>
                <h4>Clothes Iron</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/secadora.png" alt="">
                </div>
                <h4>Hair Dryer</h4>
            </div>
        </div>
        <div class="row text-gray-vd">
            <div class="col-lg-offset-2 col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/TELEFONO.png" alt="">
                </div>
                <h4>Phone</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/terraza.png" alt="">
                </div>
                <h4>Terrace</h4>
            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/jacuzzi.png" alt="">
                </div>
                <h4>Jacuzzi</h4>
            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/cocina.png" alt="">
                </div>
                <h4>Kitchen</h4>

            </div>
        </div>
        <div class="row text-gray-vd">
            <div class="col-lg-offset-2 col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/Habitaciones.png" alt="">
                </div>
                <h4>2 Bedrooms</h4>

            </div>
        </div>
    </div>
</section>

<!-- Footer Section-->
<?php include('includes/frontend/modules/footer.php')?>
<!-- jQuery-->
<script src="js/jquery-1.12.4.min.js"></script>
<!-- Bootstrap Core JavaScript-->
<script src="js/bootstrap.min.js"></script>
<!-- Plugin JavaScript-->
<script src="js/jquery.easing.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/device.min.js"></script>
<script src="js/form.min.js"></script>
<script src="js/jquery.placeholder.min.js"></script>
<script src="js/jquery.shuffle.min.js"></script>
<script src="js/jquery.parallax.min.js"></script>
<script src="js/jquery.circle-progress.min.js"></script>
<script src="js/jquery.swipebox.min.js"></script>
<script src="js/smoothscroll.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/jquery.smartmenus.js"></script>
<!-- Custom Theme JavaScript-->
<script src="js/main.js"></script>
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</body>
</html>
<!DOCTYPE html>
<html lang="es">
<?php
$title = 'Suite 7';
include('includes/frontend/modules/head.php')
?>
<body class="top" id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<!-- Preloader-->
<div id="preloader">
    <div id="status"></div>
</div>
<!-- Navigation-->
<?php include('includes/frontend/modules/nav.php')?>
<!-- Header-->
<header class="intro carousel carousel-big slide carousel-fade" id="Carousel-intro" data-ride="carousel">
    <!-- Indicators-->
    <ol class="carousel-indicators">
        <li class="active" data-target="#Carousel-intro" data-slide-to="0"></li>
        <!--<li data-target="#Carousel-intro" data-slide-to="1"></li>-->
    </ol>
    <div class="carousel-inner">
        <div class="item active">
            <div class="fill" style="background-image:url('img/rooms/7/1.jpg');">
                <div class="intro-body">
                    <h1 class="wow">SUITE 7</h1>
                    <a class="page-scroll" href="#details">
                        <span class="mouse">
                            <span>
                                <i class="icon ion-ios-arrow-down"></i>
                            </span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- Controls-->
    <a class="left carousel-control" href="#Carousel-intro" data-slide="prev">
        <span class="icon-prev"></span>
    </a>
    <a class="right carousel-control" href="#Carousel-intro"data-slide="next">
        <span class="icon-next"></span>
    </a>
</header>

<section id="details">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <p class="text-justify">Suite Deluxe with one king-size bed. The suites feature their own lounge areas, with beautiful views of the sea below and the Banderas Bay. Wide, full-size windows give the rooms an open, light feel, while the tasteful décor blends with the classic Mexican aesthetic. Decorated with local art, showcasing some of the best that Puerto Vallarta has to offer.</p>
            </div>
        </div>
        <div class="row text-gray-vd">
            <div class="col-lg-offset-2 col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/aire.png" alt="">
                </div>
                <h4>A/A</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/TV%20satelital.png" alt="">
                </div>
                <h4>Satellite TV</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/internet.png" alt="">
                </div>
                <h4>WIFI</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/NETFLIX.png" alt="">
                </div>
                <h4>NETFLIX</h4>

            </div>
        </div>
        <div class="row text-gray-vd">
            <div class="col-lg-offset-2 col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/Caja%20fuerte.png" alt="">
                </div>
                <h4>Security Box</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/Cafetera.png" alt="">
                </div>
                <h4>Coffee Maker</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/Plancha.png" alt="">
                </div>
                <h4>Clothes Iron</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/secadora.png" alt="">
                </div>
                <h4>Hair Dryer</h4>
            </div>
        </div>
        <div class="row text-gray-vd">
            <div class="col-lg-offset-2 col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/TELEFONO.png" alt="">
                </div>
                <h4>Phone</h4>

            </div>
        </div>
    </div>
</section>

<!-- Footer Section-->
<?php include('includes/frontend/modules/footer.php')?>
<!-- jQuery-->
<script src="js/jquery-1.12.4.min.js"></script>
<!-- Bootstrap Core JavaScript-->
<script src="js/bootstrap.min.js"></script>
<!-- Plugin JavaScript-->
<script src="js/jquery.easing.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/device.min.js"></script>
<script src="js/form.min.js"></script>
<script src="js/jquery.placeholder.min.js"></script>
<script src="js/jquery.shuffle.min.js"></script>
<script src="js/jquery.parallax.min.js"></script>
<script src="js/jquery.circle-progress.min.js"></script>
<script src="js/jquery.swipebox.min.js"></script>
<script src="js/smoothscroll.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/jquery.smartmenus.js"></script>
<!-- Custom Theme JavaScript-->
<script src="js/main.js"></script>
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</body>
</html>
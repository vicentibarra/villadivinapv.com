<?php
$data=array();
require(dirname(__FILE__)."/../../configure.php");
/*=========================================*/
/*			MANDA CORREO DE CONTACTO
/*=========================================*/
if(isset($_POST["action"]) && $_POST["action"]=='contact_us'){
    # Recolecta variables
    $name = $_POST['name'];
    $phone = $_POST['phone'];
    $email = $_POST['email'];
    $message = $_POST['message'];

    //Classes
    require(DIR_F_CLASSES . "class.phpmailer.php");
    require(DIR_F_CLASSES . "class.smtp.php");

    # Arma el email
    $mail_text = '
    <h1> Datos de contacto</h1>
    <p>
        <span> <b>Nombre:</b> </span>
        <span> '.$name.' </span>
    </p>
    <p>
        <span> <b>Teléfono:</b> </span>
        <span> '.$phone.' </span>
    </p>
    <p>
        <span> <b>Email:</b> </span>
        <span> '.$email.' </span>
    </p>
    <p>
        <span> <b>Mensaje:</b> </span>
        <span> '.$message.' </span>
    </p>
    ';

# Manda el correo a BIOH
    try{
        $mailer = new PHPMailer();
        $mailer->IsSMTP();
        $mailer->IsHTML(true);
        $mailer->CharSet = 'UTF-8';
        $mailer->Port = EMAIL_SENDER_PORT;
        $mailer->SMTP_PORT = EMAIL_SENDER_PORT;
        $mailer->SMTPAuth = TRUE;
        $mailer->Host = EMAIL_SENDER_SERVER;
        $mailer->Username = EMAIL_SENDER;  // Change this to your gmail adress
        $mailer->Password = EMAIL_SENDER_PASSWORD;  // Change this to your gmail password
        $mailer->From = EMAIL_SENDER;  // This HAVE TO be your gmail adress
        $mailer->FromName = 'Villa Divina WEB'; // This is the from name in the email, you can put anything you like here
        $mailer->Body = $mail_text;
        $mailer->Subject = "Mensaje de contacto de Villa Divina";
        $mailer->AddAddress(MAIL_RECEIVE_CONTACT);  // This is where you put the email adress of the person you want to mail
        $mailer->Send();
        $data['status'] = true;
    }catch (Exception $e) {
        $debug= true;
        $data['status'] = false;
    }

}//if(isset($_POST["action"]) && $_POST["action"]=='contact_us'){

/*=========================================*/
/*			MANDA CORREO DE NEWSLETTER
/*=========================================*/
if(isset($_POST["action"]) && $_POST["action"]=='newsletter'){
    # Recolecta variables
    $language = $_POST['language'];
    $email = $_POST['email'];

    //Classes
    require(DIR_F_CLASSES . "class.phpmailer.php");
    require(DIR_F_CLASSES . "class.smtp.php");

    # Arma el email
    $mail_text = '
    <h1> Datos de contacto</h1>
    <p>
        <span> <b>Email:</b> </span>
        <span> '.$email.' </span>
    </p>
    <p>
        <span> <b>Idioma:</b> </span>
        <span> '.$language.' </span>
    </p>
    ';
# Manda el correo
    try{
        $mailer = new PHPMailer();
        $mailer->IsSMTP();
        $mailer->IsHTML(true);
        $mailer->CharSet = 'UTF-8';
        $mailer->Port = EMAIL_SENDER_PORT;
        $mailer->SMTP_PORT = EMAIL_SENDER_PORT;
        $mailer->SMTPAuth = TRUE;
        $mailer->Host = EMAIL_SENDER_SERVER;
        $mailer->Username = EMAIL_SENDER;  // Change this to your gmail adress
        $mailer->Password = EMAIL_SENDER_PASSWORD;  // Change this to your gmail password
        $mailer->From = EMAIL_SENDER;  // This HAVE TO be your gmail adress
        $mailer->FromName = 'Villa Divina WEB'; // This is the from name in the email, you can put anything you like here
        $mailer->Body = $mail_text;
        $mailer->Subject = "Suscripción al newsletter de Villa Divina";
        $mailer->AddAddress(MAIL_RECEIVE_NEWSLETTER);  // This is where you put the email adress of the person you want to mail
        $mailer->Send();
        $data['status'] = true;
    }catch (Exception $e) {
        $debug= true;
        $data['status'] = false;
    }

}//if(isset($_POST["action"]) && $_POST["action"]=='newsletter'){


echo json_encode($data);

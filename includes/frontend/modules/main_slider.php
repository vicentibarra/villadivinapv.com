<header class="intro carousel carousel-big slide carousel-fade" id="Carousel-intro" data-ride="carousel">
    <!-- Indicators-->
    <ol class="carousel-indicators">
        <li class="active" data-target="#Carousel-intro" data-slide-to="0"></li>
        <li data-target="#Carousel-intro" data-slide-to="1"></li>
        <li data-target="#Carousel-intro" data-slide-to="2"></li>
        <li data-target="#Carousel-intro" data-slide-to="3"></li>
<!--        <li data-target="#Carousel-intro" data-slide-to="4"></li>-->
        <!-- <li data-target="#Carousel-intro" data-slide-to="5"></li>-->
    </ol>
    <div class="carousel-inner">
        <!--
        <div class="item active">
            <a href="https://www.villadivinapv.com/covid-test">
                <div class="fill" style="background-image:url('img/slider/banner-covid6.jpg');">
                    <div class="intro-body">
                    </div>
                </div>
            </a>

        </div>-->
        <!--        <div class="item active">-->
        <!--            <div class="fill" style="background-image:url('img/slider/promo_1.jpg');">-->
        <!--                <div class="intro-body">-->
        <!--                </div>-->
        <!--            </div>-->
        <!--        </div>-->
        <div class="item active">
            <div class="fill" style="background-image:url('img/slider/03.jpg');">
                <!-- <div class="fill" style="background-image:url('img/slider/black_friday_en.jpg');"> -->
                <div class="intro-body">
                    <!--<a class="swipebox-video" href="https://vimeo.com/153485166" data-rel="video">-->
                    <!--<h3 class="wow fadeInUp">Watch Video</h3>-->
                    <!--<h1 class="wow fadeInDown">Our Story</h1><i class="icon icon-big ion-ios-play-outline wow fadeInUp"></i>-->
                    <!--</a>-->
                </div>
            </div>
        </div>
        <div class="item">
            <div class="fill" style="background-image:url('img/slider/01.jpg');">
                <div class="intro-body">
                    <!--<h3 class="wow fadeInUp" data-wow-delay=".4s">CREATIVE & BUSINESS</h3>-->
                    <!--<h1 class="wow fadeInDown">We Are Unibody</h1>-->
                    <!--<h3 class="wow fadeInDown" data-wow-delay=".4s">1-800-unibody</h3><a class="page-scroll" href="indexslider.html#about"><span class="mouse"><span><i class="icon ion-ios-arrow-down"></i></span></span></a>-->
                </div>
            </div>
        </div>
        <div class="item">
            <div class="fill" style="background-image:url('img/slider/02.jpg');">
                <div class="intro-body">
                    <!--<h3 class="wow fadeInUp">WE HAVE MAGIC</h3>-->
                    <!--<h1 class="wow fadeInDown">Business Startup</h1>-->
                    <!--<ul class="list-inline">-->
                    <!--<li><a class="btn btn-border btn-lg page-scroll wow fadeInLeft" href="indexslider.html#about">WHO WE ARE</a></li>-->
                    <!--<li><a class="btn btn-white btn-lg page-scroll wow fadeInRight" href="indexslider.html#contact">CONTACT US</a></li>-->
                    <!--</ul>-->
                </div>
            </div>
        </div>

        <div class="item">
            <div class="fill" style="background-image:url('img/slider/04.jpg');">
                <div class="intro-body">
                    <!--<h3 class="wow fadeInUp">WE HAVE MAGIC</h3>-->
                    <!--<h1 class="wow fadeInDown">Business Startup</h1>-->
                    <!--<ul class="list-inline">-->
                    <!--<li><a class="btn btn-border btn-lg page-scroll wow fadeInLeft" href="indexslider.html#about">WHO WE ARE</a></li>-->
                    <!--<li><a class="btn btn-white btn-lg page-scroll wow fadeInRight" href="indexslider.html#contact">CONTACT US</a></li>-->
                    <!--</ul>-->
                </div>
            </div>
        </div>
    </div>
    <!-- Controls--><a class="left carousel-control" href="#Carousel-intro" data-slide="prev"><span
                class="icon-prev"></span></a><a class="right carousel-control" href="#Carousel-intro"
                                                data-slide="next"><span class="icon-next"></span></a>
</header>
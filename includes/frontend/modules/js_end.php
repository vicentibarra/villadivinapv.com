<script src="/ui/frontend/scripts/jquery.blockUI.js"></script>
<script src="/ui/frontend/scripts/jquery.validate.js"></script>
<script type="text/javascript" src="/js/lightbox.min.js"></script>
<!-- Date Range -->
<script type="text/javascript" src="/ui/frontend/scripts/vendor/daterangepicker/moment.min.js"></script>
<script type="text/javascript"
        src="/ui/frontend/scripts/vendor/daterangepicker/daterangepicker.js?ver=<?= CSS_VERSION ?>"></script>
<link rel="stylesheet" type="text/css"
      href="/ui/frontend/scripts/vendor/daterangepicker/daterangepicker.css?ver=<?= CSS_VERSION ?>"/>

<script>
    $(function () {
        /* Call Ajax */
        $(document).ajaxStart(function () {
            $.blockUI(
                {
                    message: '',
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        opacity: .5,
                        color: '#fff'
                    }
                });
        });
        $(document).ajaxStop(function () {
            $.unblockUI();
        });


        // Date Range
        var nowDate = new Date();
        var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);

        $('#daterange').daterangepicker({
            minDate: today,
            locale: {
                format: 'YYYY-MM-DD',
                separator: ' to ',
                showCancelButton: false,
                showFooterRangeInformation: false,
                showFooterTextInformation: true,
                footerTextInformationLabel: 'Lowest prices in the past 24 hours',
                cancelLabel: 'Cancel',
                applyLabel: 'Select',
                daysOfWeek: [
                    "Su",
                    "Mo",
                    "Tu",
                    "We",
                    "Th",
                    "Fr",
                    "Sa"
                ],
                monthNames: [
                    "January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"
                ],

            },
            prices: {
                '2022-11-1': 245,
                '2022-11-2': 245,
                '2022-11-3': 245,
                '2022-11-4': 245,
                '2022-11-5': 245,
                '2022-11-6': 245,
                '2022-11-7': 245,
                '2022-11-8': 245,
                '2022-11-9': 245,
                '2022-11-10': 245,
                '2022-11-11': 245,
                '2022-11-12': 245,
                '2022-11-13': 245,
                '2022-11-14': 245,
                '2022-11-15': 245,
                '2022-11-16': 244,
                '2022-11-17': 244,
                '2022-11-18': 244,
                '2022-11-19': 244,
                '2022-11-20': 244,
                '2022-11-21': 244,
                '2022-11-22': 244,
                '2022-11-23': 244,
                '2022-11-24': 244,
                '2022-11-25': 244,
                '2022-11-26': 244,
                '2022-11-27': 244,
                '2022-11-28': 244,
                '2022-11-29': 244,
                '2022-11-30': 244,
                '2022-12-1': 244,
                '2022-12-2': 244,
                '2022-12-3': 244,
                '2022-12-4': 244,
                '2022-12-5': 244,
                '2022-12-6': 244,
                '2022-12-7': 244,
                '2022-12-8': 244,
                '2022-12-9': 244,
                '2022-12-10': 244,
                '2022-12-11': 244,
                '2022-12-12': 244,
                '2022-12-13': 244,
                '2022-12-14': 244,
                '2022-12-15': 244,
                '2022-12-16': 244,
                '2022-12-17': 244,
                '2022-12-18': 244,
                '2022-12-19': 244,
                '2022-12-20': 244,
            }
        });

        $('#daterange').on('apply.daterangepicker', function (ev, picker) {
            $('#arrival').val(picker.startDate.format('YYYY-MM-DD'));
            $('#departure').val(picker.endDate.format('YYYY-MM-DD'));
        });


        $("a.example-image-link").fancybox();
        /* Valida formulario 1 */
        $("#contact_us").validate({
            rules: {
                name: {required: true, minlength: 3},
                message: {required: true, minlength: 5},
                email: {required: true, email: true, minlength: 3}
            },
            messages: {
                name: "Write your name",
                message: "Write your message",
                email: {
                    required: "Write your email",
                    email: "Is your email correct?"
                }
            },
            submitHandler: function (form) {
                $("#send_contact_form").prop("disabled", true);
                // Recaba información
                var str = $("#contact_us").serialize();
                $.ajax({
                    url: "/includes/frontend/controllers/ajax.controller.php",
                    data: str,
                    dataType: "json",
                    type: "POST",
                    success: function (msg) {
                        console.log(msg);
                        if (msg['status']) {
                            $("#contact_us_errors").html("Thank you!");
                            $("#contact_us")[0].reset();
                        } else {
                            $("#contact_us_errors").html('Error');
                        }
                        setTimeout(function () {
                            $("#contact_us_errors").html('');
                        }, 3000);

                    },
                    error: function (xhr, textStatus, errorThrown) {
                        console.log("Please report this error: " + errorThrown + xhr.status + xhr.responseText);
                    },
                    complete: function () {
                        $("#send_contact_form").prop("disabled", false)
                    }
                });

                return false;
            }
        });

        /* Enviar el formulario 1*/
        $("#send_contact_form").click(function () {
            $("#contact_us").submit();
        });

        /* Valida formulario Newsletter */
        $("#newsletter_form").validate({
            rules: {
                email: {required: true, email: true, minlength: 3}
            },
            messages: {
                email: {
                    required: "",
                    email: ""
                }
            },
            submitHandler: function (form) {
                $("#send_newsletter_form").prop("disabled", true);
                // Recaba información
                var str = $("#newsletter_form").serialize();
                $.ajax({
                    url: "/includes/frontend/controllers/ajax.controller.php",
                    data: str,
                    dataType: "json",
                    type: "POST",
                    success: function (msg) {
                        console.log(msg);
                        if (msg['status']) {
                            $("#newsletter_errors").html("Thank you!");
                            $("#newsletter_form")[0].reset();
                        } else {
                            $("#newsletter_errors").html('Error');
                        }
                        setTimeout(function () {
                            $("#newsletter_errors").html('');
                        }, 3000);

                    },
                    error: function (xhr, textStatus, errorThrown) {
                        console.log("Please report this error: " + errorThrown + xhr.status + xhr.responseText);
                    },
                    complete: function () {
                        $("#send_newsletter_form").prop("disabled", false)
                    }
                });

                return false;
            }
        });

        /* Enviar el formulario newsletter*/
        $("#send_newsletter_form").click(function () {
            $("#newsletter_form").submit();
        });

    });

</script>

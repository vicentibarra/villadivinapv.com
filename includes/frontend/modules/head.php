<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/misc/favicon.png">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="p:domain_verify" content="c8cf0ffb10e651d99ae2a47b916850a5"/>
    <title>Villa Divina Luxury Boutique - <?=@$title?></title>
    <!-- Bootstrap Core CSS-->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS-->
    <link href="css/main.css?ver=<?=CSS_VERSION?>" rel="stylesheet">
    <link href="css/datepicker.css" rel="stylesheet">
    <!--<link href="css/book-main.css" rel="stylesheet">-->
    <!--    lightbox-->
    <link rel="stylesheet" href="css/lightbox.min.css">
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-109462540-1', 'auto', {'allowLinker': true});
        ga('require', 'linker');
        ga('linker:autoLink', ['reservationtrip.com'], false, true );
        ga('send', 'pageview');

        setTimeout(function() {
            ga("send", "event", {
                eventCategory: "NoBounce",
                eventAction: "NoBounce",
                eventLabel: "Over 10 seconds"
            });
        },10*1000);

    </script>
    <!-- Hotjar Tracking Code for villadivinapv.com -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:877910,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
</head>
<nav class="navbar navbar-unibody navbar-custom navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-main-collapse"><span
                        class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span
                        class="icon-bar"></span><span
                        class="icon-bar"></span></button>
            <a class="navbar-brand page-scroll" href="/#page-top">
                <!-- Text or Image logo--><img class="logo" src="img/logo.png" alt="Logo"><img class="logodark"
                                                                                               src="img/logodark.png"
                                                                                               alt="Logo"></a>
        </div>
        <div class="collapse navbar-collapse navbar-main-collapse">
            <ul class="nav navbar-nav navbar-left">
                <li class="hidden"><a href="#page-top"></a></li>
                <li><a href="/">Home <span class="caret"></span></a>
                </li>
<!--                <li>-->
<!--                    <a href="tour">Virtual Tour <span class="caret"></span></a>-->
<!--                </li>-->
                <li><a href="rooms">Rooms <span class="caret"></span></a>

                </li>
                <li><a href="services">Services <span class="caret"></span></a>

                </li>
                <li><a href="location">Location <span class="caret"></span></a>

                </li>

                <li>
                    <a href="/es/">
                        <span class="lang">ESP</span>
                        <span class="caret"></span></a>
                </li>
            </ul>
        </div>
    </div>
</nav>
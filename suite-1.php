<!DOCTYPE html>
<html lang="en">
<?php
$title = 'Suite 1';
include('includes/frontend/modules/head.php')
?>
<body class="top" id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<!-- Preloader-->
<div id="preloader">
    <div id="status"></div>
</div>
<!-- Navigation-->
<?php include('includes/frontend/modules/nav.php')?>
<!-- Header-->
<header class="intro carousel carousel-big slide carousel-fade" id="Carousel-intro" data-ride="carousel">
    <!-- Indicators-->
    <ol class="carousel-indicators">
        <li class="active" data-target="#Carousel-intro" data-slide-to="0"></li>
        <li data-target="#Carousel-intro" data-slide-to="1"></li>
        <li data-target="#Carousel-intro" data-slide-to="2"></li>
        <li data-target="#Carousel-intro" data-slide-to="3"></li>
        <li data-target="#Carousel-intro" data-slide-to="4"></li>
        <li data-target="#Carousel-intro" data-slide-to="5"></li>
    </ol>
    <div class="carousel-inner">
        <div class="item active">
            <div class="fill" style="background-image:url('img/rooms/1/1.jpg');">
                <div class="intro-body">
                    <h1 class="wow">SUITE 1</h1>
                    <a class="page-scroll" href="#details">
                        <span class="mouse">
                            <span>
                                <i class="icon ion-ios-arrow-down"></i>
                            </span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="fill" style="background-image:url('img/rooms/1/2.jpg');">
                <div class="intro-body">
                    <h1 class="wow">SUITE 1</h1>
                    <a class="page-scroll" href="#details">
                        <span class="mouse">
                            <span>
                                <i class="icon ion-ios-arrow-down"></i>
                            </span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="fill" style="background-image:url('img/rooms/1/3.jpg');">
                <div class="intro-body">
                    <h1 class="wow">SUITE 1</h1>
                    <a class="page-scroll" href="#details">
                        <span class="mouse">
                            <span>
                                <i class="icon ion-ios-arrow-down"></i>
                            </span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="fill" style="background-image:url('img/rooms/1/4.jpg');">
                <div class="intro-body">
                    <h1 class="wow">SUITE 1</h1>
                    <a class="page-scroll" href="#details">
                        <span class="mouse">
                            <span>
                                <i class="icon ion-ios-arrow-down"></i>
                            </span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="fill" style="background-image:url('img/rooms/1/5.jpg');">
                <div class="intro-body">
                    <h1 class="wow">SUITE 1</h1>
                    <a class="page-scroll" href="#details">
                        <span class="mouse">
                            <span>
                                <i class="icon ion-ios-arrow-down"></i>
                            </span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="fill" style="background-image:url('img/rooms/1/6.jpg');">
                <div class="intro-body">
                    <h1 class="wow">SUITE 1</h1>
                    <a class="page-scroll" href="#details">
                        <span class="mouse">
                            <span>
                                <i class="icon ion-ios-arrow-down"></i>
                            </span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- Controls-->
    <a class="left carousel-control" href="#Carousel-intro" data-slide="prev">
        <span class="icon-prev"></span>
    </a>
    <a class="right carousel-control" href="#Carousel-intro"data-slide="next">
        <span class="icon-next"></span>
    </a>
</header>
<section id="details">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <!--<h3></h3>-->
                <p class="text-justify"> Villa Divina’s one and only garden room puts a bit of a twist on the standard suite. Located at the lowest part of the Villa, the room looks onto our lush and beautiful Mexican-style garden. With easy access to the outdoors and vast space, the garden room is perfect for anyone wanting to make full use of the villa’s amenities and have a greater experience of nature’s peace and tranquility.</p>
                <p class="text-justify">
                    The garden room is decorated in a contemporary style, brightened with local art. Like the standard suites, the garden room also has its own lounge seating in the garden area. The garden room also has access to all the amenities that the Villa has to offer. Like all other rooms, the garden room is pet friendly. In fact, it is the best room for any four-legged guests with its open floor and access to the garden.</p>
            </div>
        </div>
        <div class="row text-gray-vd">
            <div class="col-lg-offset-2 col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/aire.png" alt="">
                </div>
                <h4>A/C</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/TV%20satelital.png" alt="">
                </div>
                <h4>Satellite TV</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/internet.png" alt="">
                </div>
                <h4>WIFI</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/NETFLIX.png" alt="">
                </div>
                <h4>NETFLIX</h4>

            </div>
        </div>
        <div class="row text-gray-vd">
            <div class="col-lg-offset-2 col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/Caja%20fuerte.png" alt="">
                </div>
                <h4>Security Box</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/Cafetera.png" alt="">
                </div>
                <h4>Coffee Maker</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/Plancha.png" alt="">
                </div>
                <h4>Clothes Iron</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/secadora.png" alt="">
                </div>
                <h4>Hair Dryer</h4>
            </div>
        </div>
        <div class="row text-gray-vd">
            <div class="col-lg-offset-2 col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/TELEFONO.png" alt="">
                </div>
                <h4>Phone</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/terraza.png" alt="">
                </div>
                <h4>Terrace</h4>
            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/minibar.png" alt="">
                </div>
                <h4>Servibar</h4>

            </div>
        </div>
    </div>
</section>

<!-- Footer Section-->
<?php include('includes/frontend/modules/footer.php')?>
<!-- jQuery-->
<script src="js/jquery-1.12.4.min.js"></script>
<!-- Bootstrap Core JavaScript-->
<script src="js/bootstrap.min.js"></script>
<!-- Plugin JavaScript-->
<script src="js/jquery.easing.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/device.min.js"></script>
<script src="js/form.min.js"></script>
<script src="js/jquery.placeholder.min.js"></script>
<script src="js/jquery.shuffle.min.js"></script>
<script src="js/jquery.parallax.min.js"></script>
<script src="js/jquery.circle-progress.min.js"></script>
<script src="js/jquery.swipebox.min.js"></script>
<script src="js/smoothscroll.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/jquery.smartmenus.js"></script>
<!-- Custom Theme JavaScript-->
<script src="js/main.js"></script>
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</body>
</html>
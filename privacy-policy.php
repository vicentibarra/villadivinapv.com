<!DOCTYPE html>
<html lang="en">
<?php
$title = 'Services';
include('includes/frontend/modules/head.php')
?>
<body class="top" id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<!-- Preloader-->
<div id="preloader">
    <div id="status"></div>
</div>
<!-- Navigation-->
<?php
include('includes/frontend/modules/nav_wo_slider.php')
?>
<!-- Header-->
<header class="intro introhalf" data-background="img/header/1.jpg">
    <div class="intro-body">
        <h1>Policy Privacy</h1>
        <!--<h4>What we do</h4>-->
    </div>
</header>
<!-- Services Section-->
<section id="services">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h3>Policy Privacy</h3>
                <p>Effective Date: April 2018</p>
                <p>This Privacy Statement applies to www.villadivinapv.com owned and operated by Villa Divina Luxury Boutique Puerto
                    Vallarta. This Privacy Statement describes how we collect and use the information, which may include
                    personal data, you provide on our web site: www.villadivinapv.com. It also describes the choices
                    available to you regarding our use of your personal data and how you can access and update this
                    data.</p>
                <h2>Data Collection</h2>
                <p>The types of personal data that we collect include:</p>
                <p>Your first name, last name, email address, phone number and home address;</p>
                <p>Credit card details (type of card, credit card number, name on card, expiration date and security
                    code);</p>
                <p>Guest stay data, including date of arrival and departure, special requests made, observations about your
                    service preferences (including room preferences, facilities or any other services used);</p>
                <p>Data you provide regarding your marketing preferences or in the course of participating in surveys,
                    contests or promotional offers;</p>
                <p>You may always choose what personal data (if any) you wish to provide to us. If you choose not to provide
                    certain details, however, some of your transactions with us may be impacted.</p>
                <h2>Data we collect automatically</h2>
                <p>When using our website, we also collect information automatically, some of which may be personal data.
                    This includes data such as language settings, IP address, location, device settings, device OS, log
                    information, time of usage, URL requested, status report, user agent (information about the browser
                    version), operating system, result (viewer or booker), browsing history, user Booking ID, and type of
                    data viewed. We may also collect data automatically through cookies. For information on how we use
                    cookies, click here.</p>
                <h2>Processing Purposes</h2>
                <p>We use your personal data for the following purposes:</p>
                <p>A. Reservations: We use your personal data to complete and administer your online reservation.</p>
                <p>B. Customer service: We use your personal data to provide customer service.</p>
                <p>C. Guest reviews: We may use your contact data to invite you by email to write a guest review after your
                    stay. This can help other travellers to choose the accommodation that suits them best. If you submit a
                    guest review, your review may be published on our website.</p>
                <p> D. Marketing activities: We also use your data for marketing activities, as permitted by law. Where we
                    use your personal data for direct marketing purposes, such as commercial newsletters and marketing
                    communications on new products and services or other offers which we think may be of interest to you, we
                    include an unsubscribe link that you can use if you do not want us to send messages in the future.</p>
                <p>E. Other communications: There may be other times when we get in touch by email, by post, by phone or by
                    texting you, depending on the contact data you share with us. There could be a number of reasons for
                    this:</p>
                <p> a. We may need to respond to and handle requests you have made.</p>
                <p>b. If you have not finalised a reservation online, we may email you a reminder to continue with your
                    reservation. We believe that this additional service is useful to you because it allows you to carry on
                    with a reservation without having to search for the accommodation again or fill in all the reservation
                    details from scratch.</p>
                <p> c. When you use our services, we may send you a questionnaire or invite you to provide a review about
                    your experience with our website. We believe that this additional service is useful to you and to us as
                    we will be able to improve our website based on your feedback.</p>
                <p>F. Analytics, improvements and research: We use personal data to conduct research and analysis. We may
                    involve a third party to do this on our behalf. We may share or disclose the results of such research,
                    including to third-parties, in anonymous, aggregated form. We use your personal data for analytical
                    purposes, to improve our services, to enhance the user experience, and to improve the functionality and
                    quality of our online travel services.</p>
                <p>G. Security, fraud detection and prevention: We use the information, which may include personal data, in
                    order to prevent fraud and other illegal or infringing activities. We also use this information to
                    investigate and detect fraud. We can use personal data for risk assessment and security purposes,
                    including the authentication of users. For these purposes, personal data may be shared with third
                    parties, such as law enforcement authorities as permitted by applicable law and external advisors.</p>
                <p> H. Legal and compliance: In certain cases, we need to use the information provided, which may include
                    personal data, to handle and resolve legal disputes or complaints, for regulatory investigations and
                    compliance, or to enforce agreement(s) or to comply with lawful requests from law enforcement insofar as
                    it is required by law.</p>
                <p>If we use automated means to process personal data which produces legal effects or significantly affects
                    you, we will implement suitable measures to safeguard your rights and freedoms, including the right to
                    obtain human intervention.</p>
                <h2>Legal Bases</h2>
                <p>In view of purposes A and B we rely on the performance of a contract: The use of your data may be
                    necessary to perform the contract that you have with us. For example, if you use our services to make an
                    online reservation, we will use your data to carry out our obligation to complete and administer that
                    reservation under the contract that we have with you.</p>
                <p>In view of purposes C-H, we rely on its legitimate interests: We use your data for our legitimate
                    interests, such as providing you with the best appropriate content for the website, emails and
                    newsletters, to improve and promote our products and services and the content on our website, and for
                    administrative, fraud detection and legal purposes. When using personal data to serve our legitimate
                    interests, we will always balance your rights and interests in the protection of your information
                    against our rights and interests.</p>
                <p>In respect of purpose H, we also rely, where applicable, on our obligation to comply with applicable law.
                    Where needed under applicable law, we will obtain your consent prior to processing your personal data
                    for direct marketing purposes.</p>
                <p>If needed in accordance with applicable law, we will ask your consent. You can withdraw your consent
                    anytime by contacting us at any of the addresses at the end of this Privacy Statement.</p>
                <p>If you wish to object to the processing set out under C-F and no opt-out mechanism is available to you
                    directly (for instance in your account settings), to the extent applicable, please contact
                    reservaciones@villadivinapv.com.</p>

                <h2>International Data Transfers</h2>
                <p>The transmission of personal data as described in this Privacy Statement may include overseas transfers
                    of personal data to countries whose data protection laws are not as comprehensive as those of the
                    countries within the European Union. Where required by European law, we shall only transfer personal
                    data to recipients offering an adequate level of data protection. In these situations, we make
                    contractual arrangements to ensure that your personal data is still protected in line with European
                    standards. You can ask us to see a copy of these clauses using the contact details below.</p>
                
                <h2>Data Retention</h2>
                <p>We will retain your information, which may include personal data for as long as we deem it necessary to
                    provide services to you, comply with applicable laws, resolve disputes with any parties and otherwise as
                    necessary to allow us to conduct our business including to detect and prevent fraud or other illegal
                    activities. All personal data we retain will be subject to this Privacy Statement. If you have a
                    question about a specific retention period for certain types of personal data we process about you,
                    please contact us via the contact details included below.</p>
                <h2>Your choices and rights</h2>
                <p>We want you to be in control of how your personal data is used by us. You can do this in the following
                    ways:</p>
                <p>You can ask us for a copy of the personal data we hold about you;</p>
                <p>you can inform us of any changes to your personal data, or you can ask us to correct any of the personal
                    data we hold about you;</p>
                <p>in certain situations, you can ask us to erase or block or restrict the processing of the personal data
                    we hold about you, or object to particular ways in which we are using your personal data; and
                    in certain situations, you can also ask us to send the personal data you have given us to a third
                    party.</p>
                <p>Where we are using your personal data on the basis of your consent, you are entitled to withdraw that
                    consent at any time subject to applicable law. Moreover, where we process your personal data based on
                    legitimate interest or the public interest, you have the right to object at any time to that use of your
                    personal data subject to applicable law.</p>
                <p>We rely on you to ensure that your personal data is complete, accurate and current. Please do inform us
                    promptly of any changes to or inaccuracies of to your personal data by contacting
                    reservaciones@villadivinapv.com . We will handle your request in accordance with the applicable
                    law.</p>
                <h2>Questions or Complaints</h2>
                <p>If you have questions or concerns about our processing of your personal data, or if you wish to exercise
                    any of the rights you have under this notice, you are welcome to contact us via
                    reservaciones@villadivinapv.com. You may also contact your local data protection authority with
                    questions and complaints.</p>
                <h2>Changes to the Notice</h2>
                <p>Just as our business changes constantly, this Privacy Statement may also change for time to time. If you
                    want to see changes made to this Privacy Statement from time to time, we invite you to access this
                    Privacy Statement to see the changes. If we make material changes or changes that will have an impact on
                    you (e.g. when we start processing your personal data for other purposes than set out above), we will
                    contact you prior to commencing that processing.</p>
            </div>
        </div>
    </div>
</section>

<!-- Subscribe Section-->
<section class="section-small bg-img4" id="subscribe">
    <div class="overlay"></div>
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                <h3>Subscribe</h3>
                <h5>SIGN-UP TO RECEIVE FUTURE PROMOTIONS</h5>
                <!-- MailChimp Signup Form - Replace the form action in the line below with your MailChimp embed action! For more information on how to do this please visit the Docs!-->
                <form class="form-inline subscribe-form dark-form" id="mc-embedded-subscribe-form"
                      action="http://forbetterweb.us11.list-manage.com/subscribe/post?u=4f751a6c58b225179404715f0&amp;id=18fc72763a"
                      method="post" name="mc-embedded-subscribe-form" target="_blank" novalidate="">
                    <div class="input-group input-group-lg">
                        <input class="form-control" id="mce-EMAIL" type="email" name="EMAIL"
                               placeholder="Email address..."><span class="input-group-btn">
    <button class="btn btn-dark" id="mc-embedded-subscribe" type="submit" name="subscribe">Subscribe</button></span>
                        <div id="mce-responses"></div>
                        <div class="response" id="mce-error-response" style="display:none;"></div>
                        <div class="response" id="mce-success-response" style="display:none;"></div>
                    </div>
                </form>
                <!-- End MailChimp Signup Form--><img src="img/misc/mailchimp.png" alt="">
            </div>
        </div>
    </div>
</section>

<!-- Footer Section-->
<?php include('includes/frontend/modules/footer.php')?>
<!-- jQuery-->
<script src="js/jquery-1.12.4.min.js"></script>
<!-- Bootstrap Core JavaScript-->
<script src="js/bootstrap.min.js"></script>
<!-- Plugin JavaScript-->
<script src="js/jquery.easing.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/device.min.js"></script>
<script src="js/form.min.js"></script>
<script src="js/jquery.placeholder.min.js"></script>
<script src="js/jquery.shuffle.min.js"></script>
<script src="js/jquery.parallax.min.js"></script>
<script src="js/jquery.circle-progress.min.js"></script>
<script src="js/jquery.swipebox.min.js"></script>
<script src="js/smoothscroll.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/jquery.smartmenus.js"></script>
<!-- Custom Theme JavaScript-->
<script src="js/main.js"></script>
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<?php
$title = 'Services';
include('includes/frontend/modules/head.php')
?>
<body class="top" id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<!-- Preloader-->
<div id="preloader">
    <div id="status"></div>
</div>
<!-- Navigation-->
<?php
include('includes/frontend/modules/nav_wo_slider.php')
?>
<!-- Header-->
<header class="intro introhalf" data-background="img/header/1.jpg">
    <div class="intro-body">
        <h1>Services</h1>
        <!--<h4>What we do</h4>-->
    </div>
</header>
<!-- Services Section-->
<section id="services">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h3>Our Services</h3>
                <p class="text-justify">You’ll have access to all the services and amenities you’ll need for a relaxing
                    stay here in the paradise of Puerto Vallarta. All guests have access to the villa’s on-site gym,
                    replete with modern exercise gear that’ll keep you in shape while you’re on holiday. The spacious
                    kitchen means you’ll always have the option to cook for yourself, and there’s plenty of room for
                    entertaining guests. Full concierge services are also available, along with 24-hour security. All
                    rooms have access to high speed internet wi-fi, Satellite TV (Sky) and Netflix. The villa also has
                    climate control for those hot days.</p>
            </div>
        </div>
        <div class="row text-gray-vd">
            <div class="col-lg-offset-2 col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <!--<h4><i class="icon icon-big ion-music-note"></i> 7 Bedrooms</h4>-->
                <div>
                    <img class="icon icon-villa" src="img/icons/Habitaciones.png" alt="">
                </div>
                <h4>7 Bedrooms</h4>
                <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis pulvinar vestibulum. Donec-->
                <!--eleifend, sem sed dictum</p>-->
            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".8s">
                <div>
                    <img class="icon icon-villa" src="img/icons/Penthouse.png" alt="">
                </div>
                <h4> Penthouse</h4>
                <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis pulvinar vestibulum. Donec-->
                <!--eleifend, sem sed dictum</p>-->
            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".6s">
                <div>
                    <img class="icon icon-villa" src="img/icons/aire.png" alt="">
                </div>
                <h4>A/C</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".2s">
                <div>
                    <img class="icon icon-villa" src="img/icons/internet.png" alt="">
                </div>
                <h4>Wifi</h4>
            </div>

        </div>
        <div class="row text-gray-vd">
            <div class="col-lg-offset-2 col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".6s">
                <div>
                    <img class="icon icon-villa" src="img/icons/Alberca.png" alt="">
                </div>
                <h4>Pool</h4>
            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".2s">
                <div>
                    <img class="icon icon-villa" src="img/icons/jacuzzi.png" alt="">
                </div>
                <h4>Jacuzzi</h4>
            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".6s">
                <div>
                    <img class="icon icon-villa" src="img/icons/Gym.png" alt="">
                </div>
                <h4>Gym</h4>
            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".8s">
                <div>
                    <img class="icon icon-villa" src="img/icons/terraza.png" alt="">
                </div>
                <h4>Terrace</h4>

            </div>

        </div>
        <div class="row text-gray-vd">
            <div class="col-lg-offset-2 col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".2s">
                <div>
                    <img class="icon icon-villa" src="img/icons/NETFLIX.png" alt="">
                </div>
                <h4>NETFLIX</h4>
            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/TV%20satelital.png" alt="">
                </div>
                <h4>Satellite TV</h4>

            </div>


            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".6s">
                <div>
                    <img class="icon icon-villa" src="img/icons/Cafetera.png" alt="">
                </div>
                <h4>Coffee Maker</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".2s">
                <div>
                    <img class="icon icon-villa" src="img/icons/secadora.png" alt="">
                </div>
                <h4>Hair Dryer</h4>
            </div>
        </div>
        <div class="row text-gray-vd">
            <div class="col-lg-offset-2 col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/TELEFONO.png" alt="">
                </div>
                <h4>Phone</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/Caja%20fuerte.png" alt="">
                </div>
                <h4>Security Safe</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/cocina.png" alt="">
                </div>
                <h4>Kitchen</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/Plancha.png" alt="">
                </div>
                <h4>Clothes Iron</h4>

            </div>
        </div>
        <div class="row text-gray-vd">
            <div class="col-lg-offset-2 col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/Seguridad.png" alt="">
                </div>
                <h4>Security</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/musica.png" alt="">
                </div>
                <h4>MUSIC</h4>
            </div>

        </div>
    </div>
</section>
<!-- Section Hotel Services -->
<section class="section-small" id="news">
    <div class="container">
        <div class="row grid-pad">
            <div class="col-sm-6">
                <a href="food-service">
                    <h5>Food Service</h5>
                    <img class="img-responsive center-block" src="img/services/food-service/service-food.jpg" alt="">
                </a>
                <p>Our in-house food service offers succulent meals prepared by our chef on demand.</p>
                <a class="btn btn-gray btn-xs" href="food-service">Read more</a>
            </div>
            <div class="col-sm-6">
                <a href="gym">
                    <h5>Gym</h5>
                    <img class="img-responsive center-block" src="img/services/gym/1.jpg" alt="">
                </a>
                <p>Villa Divina has a fully equipped gym that will keep you healthy and motivated.</p>
                <a class="btn btn-gray btn-xs" href="gym">Read more</a>
            </div>

        </div>
    </div>
</section>
<!-- Subscribe Section-->
<section class="section-small bg-img4" id="subscribe">
    <div class="overlay"></div>
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                <h3>Subscribe</h3>
                <h5>SIGN-UP TO RECEIVE FUTURE PROMOTIONS</h5>
                <!-- MailChimp Signup Form - Replace the form action in the line below with your MailChimp embed action! For more information on how to do this please visit the Docs!-->
                <form class="form-inline subscribe-form dark-form" id="mc-embedded-subscribe-form"
                      action="http://forbetterweb.us11.list-manage.com/subscribe/post?u=4f751a6c58b225179404715f0&amp;id=18fc72763a"
                      method="post" name="mc-embedded-subscribe-form" target="_blank" novalidate="">
                    <div class="input-group input-group-lg">
                        <input class="form-control" id="mce-EMAIL" type="email" name="EMAIL"
                               placeholder="Email address..."><span class="input-group-btn">
    <button class="btn btn-dark" id="mc-embedded-subscribe" type="submit" name="subscribe">Subscribe</button></span>
                        <div id="mce-responses"></div>
                        <div class="response" id="mce-error-response" style="display:none;"></div>
                        <div class="response" id="mce-success-response" style="display:none;"></div>
                    </div>
                </form>
                <!-- End MailChimp Signup Form--><img src="img/misc/mailchimp.png" alt="">
            </div>
        </div>
    </div>
</section>

<!-- Footer Section-->
<?php include('includes/frontend/modules/footer.php')?>
<!-- jQuery-->
<script src="js/jquery-1.12.4.min.js"></script>
<!-- Bootstrap Core JavaScript-->
<script src="js/bootstrap.min.js"></script>
<!-- Plugin JavaScript-->
<script src="js/jquery.easing.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/device.min.js"></script>
<script src="js/form.min.js"></script>
<script src="js/jquery.placeholder.min.js"></script>
<script src="js/jquery.shuffle.min.js"></script>
<script src="js/jquery.parallax.min.js"></script>
<script src="js/jquery.circle-progress.min.js"></script>
<script src="js/jquery.swipebox.min.js"></script>
<script src="js/smoothscroll.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/jquery.smartmenus.js"></script>
<!-- Custom Theme JavaScript-->
<script src="js/main.js"></script>
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</body>
</html>
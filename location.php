<!DOCTYPE html>
<html lang="en">

<?php
$title='Locatio';
include('includes/frontend/modules/head.php')
?>

<body class="top" id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<!-- Preloader-->
<div id="preloader">
    <div id="status"></div>
</div>
<!-- Navigation-->
<?php include('includes/frontend/modules/nav_wo_slider.php')?>
<!-- Header-->
<header class="intro introhalf" data-background="img/header/1.jpg">
    <div class="intro-body">
        <h1>Contact us</h1>
    </div>
</header>
<!-- Contact Section-->
<section class="section-small" id="contact">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h3>How can we help you?</h3>
                <!-- Contact Form - Enter your email address on line 17 of the mail/contact_me.php file to make this form work. For more information on how to do this please visit the Docs!-->
                <form id="contact_us">
                    <div class="control-group">
                        <div class="form-group floating-label-form-group controls">
                            <label class="sr-only control-label" for="name">You Name</label>
                            <input class="form-control input-lg" name="name" type="text" placeholder="You Name">
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="form-group floating-label-form-group controls">
                            <label class="sr-only control-label" for="email">You Email</label>
                            <input class="form-control input-lg" name="email" type="email" placeholder="You Email">
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="form-group floating-label-form-group controls">
                            <label class="sr-only control-label" for="phone">You Phone</label>
                            <input class="form-control input-lg" name="phone" type="tel" placeholder="You Phone">
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="form-group floating-label-form-group controls">
                            <label class="sr-only control-label" for="message">Message</label>
                            <textarea class="form-control input-lg" name="message" rows="1" placeholder="Message">

                            </textarea>
                        </div>
                    </div>
                    <input type="hidden" name="action" value="contact_us">
                    <div id="contact_us_errors"></div>
                    <button class="btn btn-dark" type="button" id="send_contact_form">Send</button>
                </form>
            </div>
            <div class="col-md-5 col-md-offset-2">
                <h3>Toll Free USA/CAN</h3>
                <h2>
                    <i class="fa fa-phone fa-fw"></i>
                    <a href="tel:18552466553">1 855 246 6553</a>
                </h2>
                <p>Do you need information?<br>
                Do you want a quote?</p>
                <hr>
                <h5><i class="fa fa-map-marker fa-fw fa-lg"></i> Paseo de las Conchas Chinas 107, Conchas Chinas, Puerto Vallarta, México.
                </h5>
            </div>
        </div>
    </div>
</section>
<!-- Map Section-->
<section class="section-maps">
    <h3 class="text-center"> Map </h3>
    <p></p>
    <div id="map"></div>
</section>

<section class="section-maps">
    <h3 class="text-center"> Villa Divina Luxury Boutique Location </h3>
    <p></p>
    <script src="https://static.kuula.io/embed.js" data-kuula="https://kuula.co/share/7ljH9?fs=1&vr=1&iosfs=1&zoom=1&autorotate=0.21&thumbs=1&chromeless=1&logo=1&logosize=54" data-width="100%" data-height="640px"></script>
</section>
<!-- Footer Section-->
<?php include('includes/frontend/modules/footer.php')?>
<!-- jQuery-->
<script src="js/jquery-1.12.4.min.js"></script>
<!-- Bootstrap Core JavaScript-->
<script src="js/bootstrap.min.js"></script>
<!-- Plugin JavaScript-->
<script src="js/jquery.easing.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/device.min.js"></script>
<script src="js/form.min.js"></script>
<script src="js/jquery.placeholder.min.js"></script>
<script src="js/jquery.shuffle.min.js"></script>
<script src="js/jquery.parallax.min.js"></script>
<script src="js/jquery.circle-progress.min.js"></script>
<script src="js/jquery.swipebox.min.js"></script>
<script src="js/smoothscroll.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/jquery.smartmenus.js"></script>
<!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3E86i8mx1BZDlAaLcknh_mWl4F70i4os"></script>
<script src="js/map.js"></script>
<!-- Custom Theme JavaScript-->
<script src="js/main.js"></script>
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<?php include('includes/frontend/modules/js_end.php')?>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<?php
$title = 'Covid-19 Test';
include('includes/frontend/modules/head.php')
?>
<body class="top" id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<!-- Preloader-->
<div id="preloader">
    <div id="status"></div>
</div>
<!-- Navigation-->
<?php
include('includes/frontend/modules/nav_wo_slider.php')
?>
<!-- Header-->
<header class="intro introhalf" data-background="img/header/1.jpg">
    <div class="intro-body">
        <h1>COVID-19 Test</h1>
        <!--<h4>What we do</h4>-->
    </div>
</header>
<!-- Services Section-->
<section id="services">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <p>As we continue to closely monitor the latest developments surrounding COVID-19, we are aware that several countries are requiring a negative COVID-19 test result from air passengers prior to boarding an international flight to their destination.</p>

                <p>In an effort to remain true to our commitment of providing a seamless and safe travel experience for all, we have actively worked to offer convenient and affordable onsite testing options for guests having to meet those requirements.</p><p>


<h4>FOR U.S. AND CANADIAN TRAVELERS:</h4>
<p>Effective January 26, the Centers for Disease Control and Prevention (CDC) is requiring all air passengers entering the United States to present a negative COVID-19 test result. </p>

<p>

Onsite testing:<br>
<span>Antigen Test $77 USD per person</span><br>
<span>PCR Test $184 USD per person</span><br><br>

If you preferer go to get a Covid-19 Test to clinical laboratory:<br>
<span>Antigen Test $51 USD per person</span><br>
<span>PCR Test $158 USD per person</span><br>
</p>
</div>
    </div>
</section>

<!-- Subscribe Section-->
<section class="section-small bg-img4" id="subscribe">
    <div class="overlay"></div>
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                <h3>Subscribe</h3>
                <h5>SIGN-UP TO RECEIVE FUTURE PROMOTIONS</h5>
                <!-- MailChimp Signup Form - Replace the form action in the line below with your MailChimp embed action! For more information on how to do this please visit the Docs!-->
                <form class="form-inline subscribe-form dark-form" id="mc-embedded-subscribe-form"
                      action="http://forbetterweb.us11.list-manage.com/subscribe/post?u=4f751a6c58b225179404715f0&amp;id=18fc72763a"
                      method="post" name="mc-embedded-subscribe-form" target="_blank" novalidate="">
                    <div class="input-group input-group-lg">
                        <input class="form-control" id="mce-EMAIL" type="email" name="EMAIL"
                               placeholder="Email address..."><span class="input-group-btn">
    <button class="btn btn-dark" id="mc-embedded-subscribe" type="submit" name="subscribe">Subscribe</button></span>
                        <div id="mce-responses"></div>
                        <div class="response" id="mce-error-response" style="display:none;"></div>
                        <div class="response" id="mce-success-response" style="display:none;"></div>
                    </div>
                </form>
                <!-- End MailChimp Signup Form--><img src="img/misc/mailchimp.png" alt="">
            </div>
        </div>
    </div>
</section>

<!-- Footer Section-->
<?php include('includes/frontend/modules/footer.php')?>
<!-- jQuery-->
<script src="js/jquery-1.12.4.min.js"></script>
<!-- Bootstrap Core JavaScript-->
<script src="js/bootstrap.min.js"></script>
<!-- Plugin JavaScript-->
<script src="js/jquery.easing.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/device.min.js"></script>
<script src="js/form.min.js"></script>
<script src="js/jquery.placeholder.min.js"></script>
<script src="js/jquery.shuffle.min.js"></script>
<script src="js/jquery.parallax.min.js"></script>
<script src="js/jquery.circle-progress.min.js"></script>
<script src="js/jquery.swipebox.min.js"></script>
<script src="js/smoothscroll.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/jquery.smartmenus.js"></script>
<!-- Custom Theme JavaScript-->
<script src="js/main.js"></script>
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</body>
</html>

<?php include('includes/configure.php') ?>
<!DOCTYPE html>
<html lang="es">

<?php include('includes/frontend/modules/head.php') ?>

<body class="top" id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<!-- Preloader-->
<div id="preloader">
    <div id="status"></div>
</div>

<!-- Navigation-->
<?php include('includes/frontend/modules/nav.php') ?>

<!-- Intro Slider-->
<?php include('includes/frontend/modules/main_slider.php') ?>

<!-- Search Availability -->
<section id="booking-bar">
    <div class="hotel-search-con affix-top" data-spy="affix" data-offset-top="750">
        <div class="container">
            <div class="hotel-search-form">
                <form action="https://booking.reservationtrip.com/Villa_Divina/ES/" method="get">
                    <div class="row">
                        <div class="col-xs-3 text-book-or-call">
                            <span class="text-book-now">RESERVA AHORA O LLAMA</span>
                            <div class="container-date">
                                <span class="phone-search-bar">
                                    <i class="fa fa-phone fa-fw"></i>
                                    <a href="tel:013222235672" class="phone-search-bar">01 322 223 5672</a>
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-6 form-input date_flied">
                            <span class="text-white">Fechas de viaje:</span>
                            <div class="container-date">
                                <input type="hidden" name="dta" id="arrival">
                                <input type="hidden" name="dtd" id="departure">
                                <input class="date_timepicker_start" id='daterange' readonly>
                                <button type="button" value="open" class="open icon fa fa-calendar"
                                        id="icon-dta"></button>
                            </div>
                        </div>
<!--                        <div class="col-xs-3 form-input date_flied">-->
<!--                            <span class="text-white">Check-In:</span>-->
<!--                            <div class="container-date">-->
<!--                                <input placeholder="mm/dd/yy" class="date_timepicker_start" name='dta' id="dta"-->
<!--                                       readonly>-->
<!--                                <button type="button" value="open" class="open icon fa fa-calendar"-->
<!--                                        id="icon-dta"></button>-->
<!--                            </div>-->
<!--                        </div>-->
<!---->
<!--                        <div class="col-xs-3 form-input date_flied">-->
<!--                            <span class="text-white">Check-Out:</span>-->
<!--                            <div class="container-date">-->
<!--                                <input placeholder="dd/mm/yy" class="date_timepicker_end" name='dtd' id="dtd" readonly>-->
<!--                                <button type="button" value="open" class="end icon fa fa-calendar"-->
<!--                                        id="icon-dtd"></button>-->
<!--                            </div>-->
<!---->
<!--                        </div>-->

                    </div>
                    <input name="number_of_rooms" id="number_of_rooms" value="1" type="hidden">
                    <input name="radults1" id="radults" value="2" type="hidden">
                    <input name="rkids1" id="rkids" value="0" type="hidden">
                    <button class="btn btn-check-availabilitu"> ¡RESERVA AHORA! <i
                                class="icon fa fa-angle-right"></i></button>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- End Search Availability -->

<!-- About Section-->
<section class="section-small" id="about">
    <div class="container text-center">
        <div class="row">

            <div class="col-lg-8 col-lg-offset-2">

                <!--<p class="no-pad small">EST.1995</p>-->
                <!--<img src="img/vd/logo-Villa-Divina.png" alt="Flor Villa Divina">-->
                <!--<img src="img/vd/Flor-Villa-Divina.png" alt="Flor Villa Divina">-->
                <a href="https://www.tripadvisor.com.mx/Hotel_Review-g150793-d12295276-Reviews-Villa_Divina_Luxury_Boutique-Puerto_Vallarta.html">
                    <img src="img/travelers-choice-2022.png" class="img-fluid mb-5"
                         style="border: solid 2px #8b5e3c; width:200px" alt="TripAdvisor Travelers Choice 2022">
                </a>
                <h1 class="text-title">Villa Divina Luxury Boutique</h1>
                <span>Conchas Chinas, Puerto Vallarta</span>
                <p></p>
                <p class="text-justify"><b>Villa Divina Luxury Boutique </b> es más que un lugar para relajarse. Nuestro
                    objetivo es proporcionar a cada huésped una experiencia personalizada y única, ya sea una pareja que
                    busca una escapada romántica o un empresario que quiere revitalizar a su equipo. Si las vistas al
                    mar
                    de todas las habitaciones no son suficientes como para transportarle a otro mundo nuestro equipo
                    bilingüe se encargará de hacer todo lo necesario para que su sueño se haga realidad.</p>
                <p class="text-justify">
                    Ubicada en la zona conocida como Conchas Chinas, Villa Divina Hotel Boutique está a unos pocos
                    minutos en coche de espectaculares playas y vida nocturna, además del centro de Puerto Vallarta
                    con sus restaurants y bares. También hay un supermercado cerca si quiere ser independiente.
                </p>
                <!--<h2 class="classic">The Unibody Team</h2>-->
            </div>
        </div>
    </div>
</section>

<!-- Slider-->
<section class="no-pad" id="action-slider">
    <div class="carousel slide" id="carousel-light">
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <div class="container-fluid bg-gray no-pad">
                    <div class="row">
                        <div class="col-lg-6 carousel-item"><img class="img-responsive center-block"
                                                                 src="img/services/food-service.jpg" alt=""></div>
                        <div class="col-lg-3 col-lg-offset-1 carousel-item-text">
                            <h3>Servicio de comida</h3>
                            <p>Villa Divina Luxury Boutique ofrece servicio de comida a la carta por nuestro Chef
                                Profesional.</p><a
                                    class="btn btn-lg btn-violet" href="servicio-comida">Leer más!</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="container-fluid bg-gray no-pad">
                    <div class="row">
                        <div class="col-lg-6 carousel-item"><img class="img-responsive center-block"
                                                                 src="img/services/gym.jpg" alt=""></div>
                        <div class="col-lg-3 col-lg-offset-1 carousel-item-text">
                            <h3>Gym</h3>
                            <p>Villa Divina Luxury Boutique cuenta con un Gimnasio para que te mantengas en foma y
                                motivado.</p><a
                                    class="btn btn-lg btn-violet" href="gym">Leer más!</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Controls--><a class="left carousel-control" href="#carousel-light" data-slide="prev"><span
                    class="icon-prev"></span></a><a class="right carousel-control" href="#carousel-light"
                                                    data-slide="next"><span class="icon-next"></span></a>
    </div>
</section>

<!-- Testimonials Section-->
<section id="testimonials">
    <div class="container">
        <div class="row wow fadeInUp" data-wow-duration="2s" data-wow-delay=".2s">
            <div class="col-md-8 col-md-offset-2">
                <h3 class="text-center">Testimoniales</h3>
                <div class="carousel slide carousel-fade" id="carousel-testimonials" data-ride="carousel">
                    <!-- Indicators-->
                    <ol class="carousel-indicators">
                        <li class="active" data-target="#carousel-testimonials" data-slide-to="0"></li>
                        <li data-target="#carousel-testimonials" data-slide-to="1"></li>
                        <li data-target="#carousel-testimonials" data-slide-to="2"></li>
                        <li data-target="#carousel-testimonials" data-slide-to="3"></li>
                        <li data-target="#carousel-testimonials" data-slide-to="4"></li>
                        <li data-target="#carousel-testimonials" data-slide-to="5"></li>
                        <li data-target="#carousel-testimonials" data-slide-to="6"></li>
                        <li data-target="#carousel-testimonials" data-slide-to="7"></li>
                        <li data-target="#carousel-testimonials" data-slide-to="8"></li>
                    </ol>
                    <!-- Wrapper for slides-->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <a class="example-image-link" href="/img/testimonials/1-Comentario.jpg"
                               data-lightbox="example-1">
                                <img class="example-image center-block" src="/img/testimonials/1-Comentario.jpg"
                                     alt="image-1"/>
                                <div class="carousel-caption">
                                    <h2 class="classic">B&T</h2>
                                    <h5 class="no-pad">Thank you all so much, an amazing experience at this absolutely
                                        wonderful Villa. What a relaxing time we had and thank you to the awesome staff.
                                        Pascual, Angel and Carmen! the chef of course, the housekeepers were great!
                                        Enjoyed the walk with Alfredo one evening to the Villa. Adios Amigos!</h5>
                                </div>
                            </a>
                        </div>
                        <div class="item">
                            <a class="example-image-link" href="/img/testimonials/2-Comentario.jpg"
                               data-lightbox="example-2">
                                <img class="example-image center-block" src="/img/testimonials/2-Comentario.jpg"
                                     alt="image-2"/>
                                <div class="carousel-caption">
                                    <h2 class="classic">RT, C</h2>
                                    <h5 class="no-pad">Nos alegra mucho quedarnos aquí.
                                        We have had a wonderful stay. The place is beautiful. The siting is breath
                                        taking and there can be no better service anywhere. Special thanks to your
                                        talented team. Carmen, Cesar, Pascual, Victor, Alfredo + others, Muy bien hecho
                                        y gracias regresamos!
                                    </h5>
                                </div>
                            </a>
                        </div>
                        <div class="item">
                            <a class="example-image-link" href="/img/testimonials/3-Comentario.jpg"
                               data-lightbox="example-3">
                                <img class="example-image center-block" src="/img/testimonials/3-Comentario.jpg"
                                     alt="image-3"/>
                                <div class="carousel-caption">
                                    <h2 class="classic">J&A</h2>
                                    <h5 class="no-pad">This place is Amazing! Can’t wait come back here! Carmen is the
                                        best part of this stay</h5>
                                </div>
                            </a>
                        </div>
                        <div class="item">
                            <a class="example-image-link" href="/img/testimonials/4-Comentario.jpg"
                               data-lightbox="example-4">
                                <img class="example-image center-block" src="/img/testimonials/4-Comentario.jpg"
                                     alt="image-4"/>
                                <div class="carousel-caption">
                                    <h2 class="classic"></h2>
                                    <h5 class="no-pad">Nuestra estancia fue placentera, cada area fue y es lo máximo,
                                        cada
                                        rincón está lleno de tranquilidad, una vista hermosa, limpieza única, la comida
                                        es
                                        deliciosa, los momentos son agradables y Vallarta es bellísimo pero Villa Divina
                                        es
                                        aún más!</h5>
                                </div>
                            </a>
                        </div>
                        <div class="item">
                            <a class="example-image-link" href="/img/testimonials/5-Comentario.jpg"
                               data-lightbox="example-5">
                                <img class="example-image center-block" src="/img/testimonials/5-Comentario.jpg"
                                     alt="image-5"/>
                                <div class="carousel-caption">
                                    <h2 class="classic">D&J</h2>
                                    <h5 class="no-pad">What a beautiful Villa! Everyone here was so friendly and
                                        accommodating. Our suite was stunning, loved the large private balcony and comfy
                                        bed. Thank you so much for everything. The views are spectacular and we love PV.
                                    </h5>
                                </div>
                            </a>
                        </div>
                        <div class="item">
                            <a class="example-image-link" href="/img/testimonials/6-Comentario.jpg"
                               data-lightbox="example-6">
                                <img class="example-image center-block" src="/img/testimonials/6-Comentario.jpg"
                                     alt="image-6"/>
                                <div class="carousel-caption">
                                    <h2 class="classic">C&V</h2>
                                    <h5 class="no-pad">We had an excellent stay! Carmen, Ceazar and the rest of the
                                        staff
                                        were top notch! We will be back soon!
                                    </h5>
                                </div>
                            </a>
                        </div>
                        <div class="item">
                            <a class="example-image-link" href="/img/testimonials/7-Comentario.jpg"
                               data-lightbox="example-7">
                                <img class="example-image center-block" src="/img/testimonials/7-Comentario.jpg"
                                     alt="image-7"/>
                                <div class="carousel-caption">
                                    <h2 class="classic">TP</h2>
                                    <h5 class="no-pad">Everything here was amazing! We can’t say enough good things
                                        about
                                        all the staff! The Villa is better than pictures and suceeded our expectations!
                                        We
                                        can’t wait to be back! Carmen & Alfredo were cherry on the top! Thanks so
                                        much!</h5>
                                </div>
                            </a>
                        </div>
                        <div class="item">
                            <a class="example-image-link" href="/img/testimonials/8-Comentario.jpg"
                               data-lightbox="example-8">
                                <img class="example-image center-block" src="/img/testimonials/8-Comentario.jpg"
                                     alt="image-8"/>
                                <div class="carousel-caption">
                                    <h2 class="classic">A&M</h2>
                                    <h5 class="no-pad">We had a wonderful stay at the Villa. The pool was an oasis away
                                        from
                                        town. We had beautiful sunsets from room #8. If anyone is reading this looking
                                        for
                                        taco recommendations, check out sonorita’s!! their tacos were bomb. And have
                                        Céasar
                                        make you his specialty margarita here. You won’t regret it :) We will be back!
                                        Gracias a Carmen, Pascual, Ceasar + entire staff!</h5>
                                </div>
                            </a>
                        </div>
                        <div class="item">
                            <a class="example-image-link" href="/img/testimonials/9-Comentario.jpg"
                               data-lightbox="example-9">
                                <img class="example-image center-block" src="/img/testimonials/9-Comentario.jpg"
                                     alt="image-9"/>
                                <div class="carousel-caption">
                                    <h2 class="classic">J&H</h2>
                                    <h5 class="no-pad">We loved our stay in paradise, thanks to this incredible place
                                        and
                                        staff. Villa Divina is a gem and we cannot wait to return.</h5>
                                </div>
                            </a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- Subscribe Section-->
<?php include('includes/frontend/modules/suscribe.php') ?>

<!-- Footer Section-->
<?php include('includes/frontend/modules/footer.php') ?>

<!-- jQuery-->
<script src="js/jquery-1.12.4.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<!-- Bootstrap Core JavaScript-->
<script src="js/bootstrap.min.js"></script>
<!-- Plugin JavaScript-->
<script src="js/jquery.easing.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/device.min.js"></script>
<script src="js/form.min.js"></script>
<script src="js/jquery.placeholder.min.js"></script>
<script src="js/jquery.shuffle.min.js"></script>
<script src="js/jquery.parallax.min.js"></script>
<script src="js/jquery.circle-progress.min.js"></script>
<script src="js/jquery.swipebox.min.js"></script>
<script src="js/smoothscroll.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/jquery.smartmenus.js"></script>
<!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3E86i8mx1BZDlAaLcknh_mWl4F70i4os"></script>
<script src="js/map.js"></script>
<!-- Custom Theme JavaScript-->
<script src="js/main.js?ver=<?=CSS_VERSION?>"></script>
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<?php include('includes/frontend/modules/js_end.php') ?>
</body>
</html>
<section class="section-small bg-img4" id="subscribe">
    <div class="overlay"></div>
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                <h3>Suscríbete</h3>
                <h5>PARA RECIBIR NUESTRAS PROMOCIONES</h5>
                <!-- MailChimp Signup Form - Replace the form action in the line below with your MailChimp embed action! For more information on how to do this please visit the Docs!-->
                <form class="form-inline subscribe-form dark-form" id="newsletter_form"
                      method="post" name="mc-embedded-subscribe-form" target="_blank" novalidate="">
                    <div class="input-group input-group-lg">
                        <input class="form-control" id="mce-EMAIL" type="email" name="email"
                               placeholder="Email address..."><span class="input-group-btn">
                            <input type="hidden" name="language" value="es">
                            <input type="hidden" name="action" value="newsletter">
    <button class="btn btn-dark" id="send_newsletter_form" type="button" name="subscribe">Suscribir</button></span>

                    </div>
                    <div class="response" id="newsletter_errors"></div>

                </form>
                <!-- End MailChimp Signup Form--><img src="img/misc/mailchimp.png" alt="">
            </div>
        </div>
    </div>
</section>
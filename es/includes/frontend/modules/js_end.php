<script src="/ui/frontend/scripts/jquery.blockUI.js"></script>
<script src="/ui/frontend/scripts/jquery.validate.js"></script>
<script type="text/javascript" src="/js/lightbox.min.js"></script>
<!-- Date Range -->
<script type="text/javascript" src="/ui/frontend/scripts/vendor/daterangepicker/moment.min.js"></script>
<script type="text/javascript"
        src="/ui/frontend/scripts/vendor/daterangepicker/daterangepicker.js?ver=<?= CSS_VERSION ?>"></script>
<link rel="stylesheet" type="text/css"
      href="/ui/frontend/scripts/vendor/daterangepicker/daterangepicker.css?ver=<?= CSS_VERSION ?>"/>
<script>
    $(function () {
        /* Call Ajax */
        $(document).ajaxStart(function () {
            $.blockUI(
                {
                    message: '',
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        opacity: .5,
                        color: '#fff'
                    }
                });
        });
        $(document).ajaxStop(function () {
            $.unblockUI();
        });


        // Date Range
        var nowDate = new Date();
        var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);

        $('#daterange').daterangepicker({
            minDate: today,
            locale: {
                format: 'YYYY-MM-DD',
                separator: ' a ',
                showCancelButton: false,
                showFooterRangeInformation: false,
                showFooterTextInformation: true,
                footerTextInformationLabel: 'Precios más bajos en las últimas 24 horas',
                cancelLabel: 'Cancelar',
                applyLabel: 'Seleccionar',
                daysOfWeek: [
                    "Do",
                    "Lu",
                    "Ma",
                    "Mi",
                    "Ju",
                    "Vi",
                    "Sa"
                ],
                monthNames: [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agosto",
                    "Septiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                ],

            },
            prices: {
                '2022-4-18': 5331,
                '2022-4-19': 3808,
                '2022-4-20': 3808,
                '2022-4-25': 3808,
                '2022-4-26': 3808,
                '2022-4-27': 3808,
                '2022-4-28': 3808,
                '2022-4-29': 5331,
                '2022-4-30': 5331,
                '2022-5-1': 4738,
                '2022-5-2': 4738,
                '2022-5-3': 4738,
                '2022-5-4': 4738,
                '2022-5-5': 4738,
                '2022-5-6': 3808,
                '2022-5-7': 3808,
                '2022-5-8': 3808,
                '2022-5-9': 3808,
                '2022-5-10': 3808,
                '2022-5-11': 3808,
                '2022-5-15': 3808,
                '2022-5-16': 3808,
                '2022-5-17': 3808,
                '2022-5-18': 3808,
                '2022-5-22': 3808,
                '2022-5-23': 3808,
                '2022-5-24': 3808,
                '2022-5-25': 3808,
                '2022-5-26': 3808,
                '2022-5-30': 3808,
                '2022-5-31': 3808,
                '2022-6-1': 3808,
                '2022-6-12': 3808,
                '2022-6-13': 3808,
                '2022-6-14': 3808,
                '2022-6-15': 3808,
                '2022-6-16': 3808,
                '2022-6-17': 3808,
                '2022-6-18': 3808,
                '2022-6-19': 3808,
                '2022-6-20': 3808,
                '2022-6-21': 3808,
                '2022-6-22': 3808,
                '2022-6-26': 3808,
                '2022-6-27': 3808,
                '2022-6-28': 3808,
                '2022-6-29': 3808,
                '2022-6-30': 3808,
                '2022-7-1': 3808,
                '2022-7-2': 3808,
                '2022-7-3': 3808,
                '2022-7-4': 3808,
                '2022-7-5': 3808,
                '2022-7-6': 3808,
                '2022-7-7': 3808,
                '2022-7-8': 3808,
                '2022-7-9': 3808,
                '2022-7-10': 3808,
                '2022-7-11': 3808,
                '2022-7-12': 3808,
                '2022-7-13': 3808,
                '2022-7-14': 3808,
                '2022-7-15': 3808,
                '2022-7-16': 3808,
                '2022-7-17': 3808,
                '2022-7-18': 3808,
                '2022-7-19': 3808,
                '2022-7-20': 3808,
                '2022-7-21': 3808,
                '2022-7-22': 3808,
                '2022-7-23': 3808,
                '2022-7-24': 3808,
                '2022-7-25': 3808,
                '2022-7-26': 3808,
                '2022-7-27': 3808,
                '2022-7-31': 3808,
            }
        });

        $('#daterange').on('apply.daterangepicker', function (ev, picker) {
            $('#arrival').val(picker.startDate.format('YYYY-MM-DD'));
            $('#departure').val(picker.endDate.format('YYYY-MM-DD'));
        });



        /* Valida formulario 1 */
        $("#contact_us").validate({
            rules: {
                name: { required: true, minlength: 3},
                message: { required: true, minlength: 5},
                email: {required: true, email:true, minlength:3}
            },
            messages: {
                name: "Escribe un nombre",
                message: "Escribe tu pregunta o comentario",
                email: {
                    required: "Escribe un email",
                    email: "Escribe un email correcto"}
            },
            submitHandler: function(form){
                $("#send_contact_form").prop( "disabled", true );
                // Recaba información
                var str = $("#contact_us").serialize();
                $.ajax({
                    url: "/includes/frontend/controllers/ajax.controller.php",
                    data: str,
                    dataType: "json",
                    type: "POST",
                    success: function (msg) {
                        console.log(msg);
                        if(msg['status']) {
                            $("#contact_us_errors").html("Gracias");
                            $("#contact_us")[0].reset();
                        }else{
                            $("#contact_us_errors").html('No se pudo enviar el correo.');
                        }
                        setTimeout(function() {
                            $("#contact_us_errors").html('');
                        }, 3000);

                    },
                    error: function (xhr, textStatus, errorThrown) {
                        console.log("Please report this error: " + errorThrown + xhr.status + xhr.responseText);
                    },
                    complete:function(){
                        $("#send_contact_form").prop( "disabled", false)
                    }
                });

                return false;
            }
        });

        /* Enviar el formulario 1*/
        $("#send_contact_form").click(function () {
            $("#contact_us").submit();
        });

        /* Valida formulario Newsletter */
        $("#newsletter_form").validate({
            rules: {
                email: {required: true, email:true, minlength:3}
            },
            messages: {
                email: {
                    required: "",
                    email: ""}
            },
            submitHandler: function(form){
                $("#send_newsletter_form").prop( "disabled", true );
                // Recaba información
                var str = $("#newsletter_form").serialize();
                $.ajax({
                    url: "/includes/frontend/controllers/ajax.controller.php",
                    data: str,
                    dataType: "json",
                    type: "POST",
                    success: function (msg) {
                        console.log(msg);
                        if(msg['status']) {
                            $("#newsletter_errors").html("Gracias!");
                            $("#newsletter_form")[0].reset();
                        }else{
                            $("#newsletter_errors").html('Error');
                        }
                        setTimeout(function() {
                            $("#newsletter_errors").html('');
                        }, 3000);

                    },
                    error: function (xhr, textStatus, errorThrown) {
                        console.log("Please report this error: " + errorThrown + xhr.status + xhr.responseText);
                    },
                    complete:function(){
                        $("#send_newsletter_form").prop( "disabled", false)
                    }
                });

                return false;
            }
        });

        /* Enviar el formulario newsletter*/
        $("#send_newsletter_form").click(function () {
            $("#newsletter_form").submit();
        });

    });

</script>

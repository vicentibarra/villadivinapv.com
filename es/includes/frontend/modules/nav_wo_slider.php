<nav class="navbar navbar-unibody navbar-custom navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-main-collapse"><span
                        class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span
                        class="icon-bar"></span></button>
            <a class="navbar-brand page-scroll" href="/#page-top">
                <!-- Text or Image logo--><img class="logo" src="img/logo.png" alt="Logo"><img class="logodark"
                                                                                               src="img/logodark.png"
                                                                                               alt="Logo"></a>
        </div>
        <div class="collapse navbar-collapse navbar-main-collapse">
            <ul class="nav navbar-nav navbar-left">
                <li class="hidden"><a href="#page-top"></a></li>
                <li class="active"><a href="/es/">Home <span class="caret"></span></a>
                </li>
<!--                <li>-->
<!--                    <a href="tour">Tour Virtual<span class="caret"></span></a>-->
<!--                </li>-->
                <li><a href="suites">Suites <span class="caret"></span></a>

                </li>
                <li><a href="servicios">Servicios <span class="caret"></span></a>

                </li>
                <li><a href="ubicacion">Ubicacion <span class="caret"></span></a>

                </li>

                <li>
                    <a href="/">
                        <span class="lang">ENG</span>
                        <span class="caret"></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<?php

define('APP_FOLDER', '/');//Physical folder where files are saved.

// URLS AND PATHS SETUP
define('ROOT',$_SERVER['DOCUMENT_ROOT'].APP_FOLDER);
define('HOST',$_SERVER['HTTP_HOST'].APP_FOLDER);
define('HTTP','http://');
define('HTTPS','https://');
define('CSS_VERSION',1.256332);



// SHARED PATHS
define('DIR_UI',APP_FOLDER.'ui/');
define('DIR_INCLUDES',ROOT.'includes/');
define('LOAD_INCLUDES',HTTP.HOST.'includes/');
define('DIR_AUTH',DIR_INCLUDES.'auth/');
define('DIR_MEDIA',HOST.'media/');
define('ROOT_MEDIA',ROOT.'media/');
// SHARED PATHS
define('DIR_C_UI',DIR_UI.'common/');
define('DIR_C_INCLUDES',DIR_INCLUDES.'common/');
// SHARED UI paths
define('DIR_C_STYLES',DIR_C_UI.'styles/');
define('DIR_C_SCRIPTS',DIR_C_UI.'scripts/');
define('DIR_C_IMG',DIR_C_UI.'img/');
// Shared CORE paths
define('DIR_C_CLASSES',DIR_C_INCLUDES.'classes/');
define('DIR_C_LIBRARIES',DIR_C_INCLUDES.'libraries/');
define('DIR_C_MODULES',DIR_C_INCLUDES.'modules/');
define('DIR_C_VIEWS',DIR_C_INCLUDES.'views/');

// FRONTEND PATHS
define('DIR_F_UI',DIR_UI.'frontend/');
define('DIR_F_INCLUDES',DIR_INCLUDES.'frontend/');
// Frontend UI paths
define('DIR_F_STYLES',DIR_F_UI.'styles/');
define('DIR_F_SCRIPTS',DIR_F_UI.'scripts/');
define('DIR_F_IMG',DIR_F_UI.'images/');
define('DIR_F_360',DIR_F_UI.'360/');
// Frontend CORE paths
define('DIR_F_CLASSES',DIR_F_INCLUDES.'classes/');
define('DIR_F_LIBRARIES',DIR_F_INCLUDES.'libraries/');
define('DIR_F_MODULES',DIR_F_INCLUDES.'modules/');
define('DIR_F_VIEWS',DIR_F_INCLUDES.'views/');

// BACKEND PATHS
define('DIR_B_UI',DIR_UI.'backend/');
define('DIR_B_INCLUDES',DIR_INCLUDES.'backend/');
// Backend UI paths
define('DIR_B_STYLES',DIR_B_UI.'styles/');
define('DIR_B_SCRIPTS',DIR_B_UI.'scripts/');
define('DIR_B_IMG',DIR_B_UI.'img/');
// Backend CORE paths
define('DIR_B_CLASSES',DIR_B_INCLUDES.'classes/');
define('DIR_B_LIBRARIES',DIR_B_INCLUDES.'libraries/');
define('DIR_B_MODULES',DIR_B_INCLUDES.'modules/');
define('DIR_B_VIEWS',DIR_B_INCLUDES.'views/');
define('DIR_B_CONTROLLERS',DIR_B_INCLUDES.'controllers/');
define('DIR_B_SNIPPETS',DIR_B_INCLUDES.'snippets/');
// Backend LOAD paths
define('LOAD_B_TOOLS',HTTP.str_replace('//','/',HOST.APP_URI).'tools/');
define('LOAD_B_INCLUDES',LOAD_INCLUDES.'backend/');
define('LOAD_B_MODULES',LOAD_B_INCLUDES.'modules/');
define('LOAD_B_CONTROLLERS',LOAD_B_INCLUDES.'controllers/');
define('LOAD_F_INCLUDES',LOAD_INCLUDES.'frontend/');
define('LOAD_F_CONTROLLERS',LOAD_F_INCLUDES.'controllers/');
define('LOAD_C_INCLUDES',LOAD_INCLUDES.'common/');
/* Datos smtp para enviar correos  */
define("EMAIL_SENDER","webmaster@hotelsanmarino.com");
define("EMAIL_SENDER_PASSWORD","webGEhsm2011");
define("EMAIL_SENDER_PORT",587);
define("EMAIL_SENDER_SERVER","gator4111.hostgator.com");

# Mails para recibir los correos de la pagina y del newsletter
define('MAIL_RECEIVE_CONTACT',"ecommerce@villadivinapv.com");
define('MAIL_RECEIVE_NEWSLETTER',"ecommerce@villadivinapv.com");


?>
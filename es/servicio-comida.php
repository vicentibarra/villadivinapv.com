<!DOCTYPE html>
<html lang="es">

<?php
$title='Servicio de Comida';
include('includes/frontend/modules/head.php')
?>

<body class="top" id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<!-- Preloader-->
<div id="preloader">
    <div id="status"></div>
</div>
<!-- Navigation-->
<?php include('includes/frontend/modules/nav.php')?>
<!-- Header-->
<header class="intro carousel carousel-big slide carousel-fade" id="Carousel-intro" data-ride="carousel">
    <!-- Indicators-->
    <ol class="carousel-indicators">
        <li class="active" data-target="#Carousel-intro" data-slide-to="0"></li>
        <li data-target="#Carousel-intro" data-slide-to="1"></li>
        <li data-target="#Carousel-intro" data-slide-to="2"></li>
        <li data-target="#Carousel-intro" data-slide-to="3"></li>
        <li data-target="#Carousel-intro" data-slide-to="4"></li>
        <li data-target="#Carousel-intro" data-slide-to="5"></li>
    </ol>
    <div class="carousel-inner">
        <div class="item active">
            <div class="fill" style="background-image:url('img/services/food-service/1.jpg');">
                <div class="intro-body">
                    <!--<h1 class="wow">SUITE 1</h1>-->
                    <a class="page-scroll" href="#details">
                        <span class="mouse">
                            <span>
                                <i class="icon ion-ios-arrow-down"></i>
                            </span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="fill" style="background-image:url('img/services/food-service/2.jpg');">
                <div class="intro-body">
                    <!--<h1 class="wow">SUITE 1</h1>-->
                    <a class="page-scroll" href="#details">
                        <span class="mouse">
                            <span>
                                <i class="icon ion-ios-arrow-down"></i>
                            </span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="fill" style="background-image:url('img/services/food-service/3.jpg');">
                <div class="intro-body">
                    <!--<h1 class="wow">SUITE 1</h1>-->
                    <a class="page-scroll" href="#details">
                        <span class="mouse">
                            <span>
                                <i class="icon ion-ios-arrow-down"></i>
                            </span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="fill" style="background-image:url('img/services/food-service/4.jpg');">
                <div class="intro-body">
                    <!--<h1 class="wow">SUITE 1</h1>-->
                    <a class="page-scroll" href="#details">
                        <span class="mouse">
                            <span>
                                <i class="icon ion-ios-arrow-down"></i>
                            </span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="fill" style="background-image:url('img/services/food-service/5.jpg');">
                <div class="intro-body">
                    <!--<h1 class="wow">SUITE 1</h1>-->
                    <a class="page-scroll" href="#details">
                        <span class="mouse">
                            <span>
                                <i class="icon ion-ios-arrow-down"></i>
                            </span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="fill" style="background-image:url('img/services/food-service/6.jpg');">
                <div class="intro-body">
                    <!--<h1 class="wow">SUITE 1</h1>-->
                    <a class="page-scroll" href="#details">
                        <span class="mouse">
                            <span>
                                <i class="icon ion-ios-arrow-down"></i>
                            </span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- Controls-->
    <a class="left carousel-control" href="#Carousel-intro" data-slide="prev">
        <span class="icon-prev"></span>
    </a>
    <a class="right carousel-control" href="#Carousel-intro"data-slide="next">
        <span class="icon-next"></span>
    </a>
</header>

<section id="details">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <p class="text-justify">
                    Villa Divina Luxury Boutique ofrece servicio de comida a la carta por nuestro Chef Profesional. <br>
                    Desayunar con una espectacular vista al mar es la mejor manera de comenzar el día. También tenemos servicio de comidas y cenas.
                </p>
            </div>
        </div>
    </div>
</section>

<!-- Footer Section-->
<?php include('includes/frontend/modules/footer.php')?>
<!-- jQuery-->
<script src="js/jquery-1.12.4.min.js"></script>
<!-- Bootstrap Core JavaScript-->
<script src="js/bootstrap.min.js"></script>
<!-- Plugin JavaScript-->
<script src="js/jquery.easing.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/device.min.js"></script>
<script src="js/form.min.js"></script>
<script src="js/jquery.placeholder.min.js"></script>
<script src="js/jquery.shuffle.min.js"></script>
<script src="js/jquery.parallax.min.js"></script>
<script src="js/jquery.circle-progress.min.js"></script>
<script src="js/jquery.swipebox.min.js"></script>
<script src="js/smoothscroll.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/jquery.smartmenus.js"></script>
<!-- Custom Theme JavaScript-->
<script src="js/main.js"></script>
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</body>
</html>
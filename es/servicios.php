<!DOCTYPE html>
<html lang="es">
<?php
$title = 'Servicios';
include('includes/frontend/modules/head.php')
?>
<body class="top" id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<!-- Preloader-->
<div id="preloader">
    <div id="status"></div>
</div>
<!-- Navigation-->
<?php
include('includes/frontend/modules/nav_wo_slider.php')
?>
<!-- Header-->
<header class="intro introhalf" data-background="img/header/1.jpg">
    <div class="intro-body">
        <h1>Servicios</h1>
        <!--<h4>What we do</h4>-->
    </div>
</header>

<!-- Services Section-->
<section id="services">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h3>Nuestros Servicios</h3>
                <p class="text-justify">
                    En Villa Divina tiene el acceso a todos los servicios y amenidades que necesita para una estancia cómoda
                    y relajada en Puerto Vallarta. Estos incluyen un gimnasio con equipo moderno para estar en forma durante
                    las vacaciones, una cocina amplia y completa que le permitiría preparar alimentos si así lo desea,
                    además de seguridad día y noche. Todas las habitaciones tienen acceso a Internet de alta velocidad
                    inálambrico, Televisón Satelital (SKY) y Netflix.</p>
            </div>
        </div>
        <div class="row text-gray-vd">
            <div class="col-lg-offset-2 col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <!--<h4><i class="icon icon-big ion-music-note"></i> 7 Bedrooms</h4>-->
                <div>
                    <img class="icon icon-villa" src="img/icons/Habitaciones.png" alt="">
                </div>
                <h4>7 Habitaciones</h4>
                <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis pulvinar vestibulum. Donec-->
                <!--eleifend, sem sed dictum</p>-->
            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".8s">
                <div>
                    <img class="icon icon-villa" src="img/icons/Penthouse.png" alt="">
                </div>
                <h4> Penthouse</h4>
                <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis pulvinar vestibulum. Donec-->
                <!--eleifend, sem sed dictum</p>-->
            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".6s">
                <div>
                    <img class="icon icon-villa" src="img/icons/aire.png" alt="">
                </div>
                <h4>A/A</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".2s">
                <div>
                    <img class="icon icon-villa" src="img/icons/internet.png" alt="">
                </div>
                <h4>Wifi</h4>
            </div>

        </div>
        <div class="row text-gray-vd">
            <div class="col-lg-offset-2 col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".6s">
                <div>
                    <img class="icon icon-villa" src="img/icons/Alberca.png" alt="">
                </div>
                <h4>Alberca</h4>
            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".2s">
                <div>
                    <img class="icon icon-villa" src="img/icons/jacuzzi.png" alt="">
                </div>
                <h4>Jacuzzi</h4>
            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".6s">
                <div>
                    <img class="icon icon-villa" src="img/icons/Gym.png" alt="">
                </div>
                <h4>Gym</h4>
            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".8s">
                <div>
                    <img class="icon icon-villa" src="img/icons/terraza.png" alt="">
                </div>
                <h4>Terraza</h4>

            </div>

        </div>
        <div class="row text-gray-vd">
            <div class="col-lg-offset-2 col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".2s">
                <div>
                    <img class="icon icon-villa" src="img/icons/NETFLIX.png" alt="">
                </div>
                <h4>NETFLIX</h4>
            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/TV%20satelital.png" alt="">
                </div>
                <h4>TV Satelital</h4>

            </div>


            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".6s">
                <div>
                    <img class="icon icon-villa" src="img/icons/Cafetera.png" alt="">
                </div>
                <h4>Cafetera</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".2s">
                <div>
                    <img class="icon icon-villa" src="img/icons/secadora.png" alt="">
                </div>
                <h4>Secadora</h4>
            </div>
        </div>
        <div class="row text-gray-vd">
            <div class="col-lg-offset-2 col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/TELEFONO.png" alt="">
                </div>
                <h4>Teléfono</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/Caja%20fuerte.png" alt="">
                </div>
                <h4>Caja de Seguridad</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/cocina.png" alt="">
                </div>
                <h4>Cocina</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/Plancha.png" alt="">
                </div>
                <h4>Plancha</h4>

            </div>
        </div>
        <div class="row text-gray-vd">
            <div class="col-lg-offset-2 col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/Seguridad.png" alt="">
                </div>
                <h4>Seguridad</h4>

            </div>
            <div class="col-lg-2 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div>
                    <img class="icon icon-villa" src="img/icons/musica.png" alt="">
                </div>
                <h4>Música</h4>
            </div>

        </div>
    </div>
</section>
<!-- Section Hotel Services -->
<section class="section-small" id="news">
    <div class="container">
        <div class="row grid-pad">
            <div class="col-sm-6">
                <a href="servicio-comida">
                    <h5>Servicio de Comida</h5>
                    <img class="img-responsive center-block" src="img/services/food-service/service-food.jpg" alt="">
                </a>
                <p>Villa Divina Luxury Boutique ofrece servicio de comida a la carta por nuestro Chef Profesional.</p>
                <a class="btn btn-gray btn-xs" href="servicio-comida">Leer más!</a>
            </div>
            <div class="col-sm-6">
                <a href="gym">
                    <h5>Gimnasio</h5>
                    <img class="img-responsive center-block" src="img/services/gym/1.jpg" alt="">
                </a>
                <p>Villa Divina Luxury Boutique cuenta con un Gimnasio para que te mantengas en foma y motivado.</p>
                <a class="btn btn-gray btn-xs" href="gym">Leer más!</a>
            </div>

        </div>
    </div>
</section>


<!-- Subscribe Section-->
<section class="section-small bg-img4" id="subscribe">
    <div class="overlay"></div>
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                <h3>Subscribe</h3>
                <h5>SIGN-UP TO RECEIVE FUTURE PROMOTIONS</h5>
                <!-- MailChimp Signup Form - Replace the form action in the line below with your MailChimp embed action! For more information on how to do this please visit the Docs!-->
                <form class="form-inline subscribe-form dark-form" id="mc-embedded-subscribe-form"
                      action="http://forbetterweb.us11.list-manage.com/subscribe/post?u=4f751a6c58b225179404715f0&amp;id=18fc72763a"
                      method="post" name="mc-embedded-subscribe-form" target="_blank" novalidate="">
                    <div class="input-group input-group-lg">
                        <input class="form-control" id="mce-EMAIL" type="email" name="EMAIL"
                               placeholder="Email address..."><span class="input-group-btn">
    <button class="btn btn-dark" id="mc-embedded-subscribe" type="submit" name="subscribe">Subscribe</button></span>
                        <div id="mce-responses"></div>
                        <div class="response" id="mce-error-response" style="display:none;"></div>
                        <div class="response" id="mce-success-response" style="display:none;"></div>
                    </div>
                </form>
                <!-- End MailChimp Signup Form--><img src="img/misc/mailchimp.png" alt="">
            </div>
        </div>
    </div>
</section>

<!-- Footer Section-->
<?php include('includes/frontend/modules/footer.php')?>
<!-- jQuery-->
<script src="js/jquery-1.12.4.min.js"></script>
<!-- Bootstrap Core JavaScript-->
<script src="js/bootstrap.min.js"></script>
<!-- Plugin JavaScript-->
<script src="js/jquery.easing.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/device.min.js"></script>
<script src="js/form.min.js"></script>
<script src="js/jquery.placeholder.min.js"></script>
<script src="js/jquery.shuffle.min.js"></script>
<script src="js/jquery.parallax.min.js"></script>
<script src="js/jquery.circle-progress.min.js"></script>
<script src="js/jquery.swipebox.min.js"></script>
<script src="js/smoothscroll.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/jquery.smartmenus.js"></script>
<!-- Custom Theme JavaScript-->
<script src="js/main.js"></script>
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</body>
</html>
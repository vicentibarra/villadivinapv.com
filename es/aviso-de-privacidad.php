<!DOCTYPE html>
<html lang="es">
<?php
$title = 'Services';
include('includes/frontend/modules/head.php')
?>
<body class="top" id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<!-- Preloader-->
<div id="preloader">
    <div id="status"></div>
</div>
<!-- Navigation-->
<?php
include('includes/frontend/modules/nav_wo_slider.php')
?>
<!-- Header-->
<header class="intro introhalf" data-background="img/header/1.jpg">
    <div class="intro-body">
        <h1>Aviso de privacidad</h1>
    </div>
</header>
<!-- Services Section-->
<section id="services">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h3>Aviso de privacidad</h3>
                <p> Fecha de entrada en vigor: abril de 2018</p>
                <p>Esta Política de privacidad es aplicable a www.villadivinapv.com, que pertenece a y está gestionada
                    por Villa Divina Luxury Boutique Puerto Vallarta. Esta Política de privacidad describe cómo recopilamos y utilizamos
                    la información, que puede incluir datos personales, que nos proporcionas en nuestra web:
                    www.villadivinapv.com. También describe las opciones que tienes respecto al uso de tus datos
                    personales y de qué manera puedes acceder y actualizar estos datos.</p>
                <h2>Recopilación de datos</h2>
                <p>Estos son algunos de los tipos de información personal que recopilamos:</p>
                <p>Nombre, apellidos, e-mail, número de teléfono y dirección postal;</p>
                <p>Datos de tarjetas de crédito (tipo de tarjeta, número de tarjeta, nombre del titular, fecha de caducidad
                    y código de seguridad);</p>
                <p>Información sobre las estancias de los clientes, como las fechas de llegada y salida, peticiones
                    especiales, observaciones sobre tus preferencias en los servicios (habitaciones, instalaciones o
                    cualquier otro servicio utilizado);</p>
                <p>Información relacionada con tus preferencias de marketing o que nos facilitas cuando participas en
                    encuestas, concursos y ofertas promocionales;</p>
                <p>Puedes elegir siempre los datos personales (si los hay) que nos quieres proporcionar. Sin embargo, si
                    eliges no proporcionarnos algunos datos, puede influir en algunas de tus operaciones.</p>
                <h2>Los datos que recopilamos de forma automática</h2>
                <p>Cuando utilizas nuestra web, también recopilamos datos de forma automática, algunos pueden ser datos
                    personales. Por ejemplo, datos como preferencia de idioma, dirección IP, ubicación, ajustes del
                    dispositivo, sistema operativo del dispositivo, datos de registro, tiempo de uso, URL solicitada,
                    informe de estado, agente de usuario (información sobre la versión del navegador), sistema operativo,
                    resultado (visita o reserva), historial de navegación, ID de reserva del usuario y tipo de información
                    vista. También podemos recopilar datos automáticamente mediante las cookies. Para más información sobre
                    cómo utilizar cookies, haz clic aquí.</p>
                <h2>Fines del procesamiento</h2>
                <p>Utilizamos tus datos personales para los siguientes fines:</p>
                <p>A. Reservas: utilizamos tus datos personales para completar y gestionar tu reserva online.</p>
                <p> B. Atención al cliente: utilizamos tus datos personales para ofrecer atención al cliente.</p>
                <p>C. Comentarios de los clientes: puede que utilicemos tus datos de contacto para invitarte por e-mail a
                    escribir un comentario después de la estancia. De esta manera ayudas a otros viajeros a elegir el
                    alojamiento que mejor les encaja. Si envías un comentario, puede publicarse en nuestra web.</p>
                <p>D. Actividades de marketing: también utilizamos tus datos para actividades relacionadas con el marketing,
                    según lo permitido por la ley. Cuando utilizamos tus datos personales para hacer marketing directo, como
                    newsletters comerciales y comunicaciones de marketing sobre servicios y productos nuevos u otras ofertas
                    que creemos que te pueden interesar, incluimos un enlace para que te des de baja, que puedes utilizar si
                    no quieres que te enviemos más mensajes en el futuro.</p>
                <p>E. Otras comunicaciones: puede que nos pongamos en contacto contigo por e-mail, por correo, por teléfono
                    o enviando un mensaje en función de los datos de contacto que nos dejes. Los motivos pueden ser:</p>
                <p>a. Puede que tengamos que responder o gestionar peticiones que nos has hecho.</p>
                <p>b. Si no has completado una reserva online, puede que te enviemos un e-mail recordatorio para seguir con
                    tu reserva. Creemos que este servicio extra es útil para ti porque te permite completar una reserva sin
                    tener que volver a buscar el alojamiento otra vez y sin volver a rellenar los datos de la reserva desde
                    el principio.</p>
                <p>c. Cuando utilizas nuestros servicios, puede que te enviemos un cuestionario o te invitemos a escribir un
                    comentario sobre tu experiencia en nuestra web. Creemos que este servicio extra es útil para ti y para
                    nosotros, así podremos mejorar nuestra página web según lo que nos cuentes.</p>
                <p>F. Análisis, mejoras e investigación: utilizamos los datos personales para llevar a cabo investigaciones
                    y análisis. Puede que utilicemos a un tercero para que lo lleve a cabo en nuestro nombre. Puede que
                    compartamos o revelemos los resultados de las investigaciones, incluidas terceras partes, de forma
                    anónima y global. Utilizamos los datos personales para realizar análisis, mejorar nuestros servicios,
                    ofrecer una mejor experiencia de usuario, ampliar las funcionalidades y conseguir más calidad en
                    nuestros servicios de viaje online.</p>
                <p>G. Seguridad, detección de fraude y prevención: utilizamos la información, que puede incluir datos
                    personales, para prevenir el fraude y otras actividades ilegales o ilícitas. También utilizamos esta
                    información para investigar y detectar el fraude. Puede que usemos datos personales para evaluar riesgos
                    y para fines de seguridad, como la autenticación de usuarios. Con estos fines, puede que compartamos los
                    datos personales con terceros, como por ejemplo autoridades encargadas del cumplimiento de la ley, según
                    lo permitido por la legislación aplicable y asesores externos.</p>
                <p>H. Materia legal y de cumplimiento normativo: en algunos casos, debemos utilizar la información
                    proporcionada, que puede incluir datos personales, para gestionar y resolver reclamaciones o litigios
                    legales, para cumplir la normativa o para investigaciones sobre regulación, o para ejecutar acuerdos o
                    cumplir peticiones legales de aplicación de la ley en la medida en que la legislación lo requiera.</p>
                <p>Si utilizamos medios automáticos para procesar datos personales que tengan efectos legales o que puedan
                    afectarte de forma significativa, aplicaremos las medidas necesarias para salvaguardar tus derechos y
                    libertades, incluido el derecho a obtener intervención humana.</p>
                <h2>Bases legales</h2>
                <p>Habida cuenta de los fines A y B, dependemos del cumplimiento de un contracto: el uso de tus datos puede
                    ser necesario para cumplir con el contrato que tienes con nosotros. Por ejemplo, si utilizas nuestros
                    servicios para hacer una reserva online, usaremos tus datos para llevar a cabo nuestra obligación de
                    completar y gestionar la reserva según el contrato que tenemos contigo.</p>
                <p>Habida cuenta de los fines de la C a la H, dependemos de los intereses legítimos: utilizamos tus datos
                    para nuestros intereses legítimos, como ofrecerte el contenido más adecuado para la web, e-mails y
                    newsletters, para mejorar y promocionar nuestros productos y servicios, y el contenido de nuestra web, y
                    para la detección del fraude administrativo y fines legales. Al utilizar los datos personales para
                    servir nuestros intereses legítimos, pondremos siempre tus derechos e intereses en la protección de tus
                    datos por encima de nuestros derechos e intereses.</p>
                <p>Respecto al fin H, dependemos también, cuando sea aplicable, de nuestra obligación de cumplir con la
                    legislación aplicable.</p>
                <p>Cuando sea necesario según la legislación aplicable, solicitaremos tu consentimiento previo para procesar
                    tus datos personales con fines de marketing directo.</p>
                <p>Si fuera necesario según la legislación aplicable, solicitaremos tu consentimiento. Puedes retirar tu
                    consentimiento en cualquier momento escribiendo a cualquiera de las direcciones que aparecen al final de
                    esta Política de privacidad.</p>
                <p>Si quieres oponerte al proceso establecido en los puntos C-F y no tienes ningún medio directo a tu
                    alcance para rechazarlo (por ejemplo en la configuración de tu cuenta), en la medida en que sea
                    aplicable, ponte en contacto con reservaciones@villadivinapv.com.</p>

                <h2>Transferencias de datos internacionales</h2>
                <p>La transmisión de datos personales tal y como se describe en esta Política de privacidad puede incluir
                    transferencias de datos personales al extranjero, a países cuya legislación en materia de protección de
                    datos no es tan exhaustiva como la de los países de la Unión Europea. Cuando la legislación europea lo
                    requiera, solo transferiremos datos personales a destinatarios que ofrezcan un nivel de protección de
                    datos adecuado. En estas situaciones, realizamos acuerdos contractuales para garantizar que tus datos
                    personales siguen estando protegidos de acuerdo con la normativa europea. Si quieres ver una copia de
                    estas cláusulas, puedes solicitarlo utilizando los datos de contacto más abajo.</p>
                
                <h2>Retención de datos</h2>
                <p>Retendremos tu información, que puede que incluya datos personales, durante el tiempo que lo consideremos
                    necesario para proporcionarte servicios, cumplir con la legislación aplicable, resolver litigios con
                    otras partes y por cualquier otro motivo necesario que nos permita desempeñar nuestras actividades,
                    incluida la detección y prevención del fraude u otras actividades ilegales. Todos los datos personales
                    que retenemos estarán sujetos a esta Política de privacidad. Si tienes alguna pregunta sobre un periodo
                    de retención concreto para algunos tipos de datos personales que procesamos sobre ti, ponte en contacto
                    con nosotros con los datos de contacto que se incluyen más abajo.</p>
                <h2>Tus opciones y derechos</h2>
                <p>Queremos que tengas el control de cómo utilizamos tus datos personales. Lo puedes hacer de las siguientes
                    formas:</p>
                <p>nos puedes pedir una copia de los datos personales que tenemos sobre ti;</p>
                <p>nos puedes informar de cualquier cambio en tus datos personales, o puedes pedirnos que corrijamos
                    cualquier dato personal que tengamos sobre ti;</p>
                <p>en algunas situaciones, nos puedes pedir que eliminemos, bloqueemos o restrinjamos el procesamiento de
                    los datos personales que tenemos sobre ti, o puedes oponerte a que utilicemos tus datos personales de
                    alguna forma en concreto; y</p>
                <p>en algunas situaciones, también puedes pedirnos que enviemos los datos personales que nos has dado a un
                    tercero.</p>
                <p> Cuando estemos utilizando tus datos personales bajo tu consentimiento, tienes el derecho de retirar ese
                    consentimiento en cualquier momento sujeto a la legislación aplicable. Además, cuando estemos procesando
                    tus datos personales basándonos en el interés legítimo o en el interés público, tienes el derecho de
                    oponerte en cualquier momento a ese uso de tus datos personales sujeto a la legislación aplicable.</p>
                <p>Dependemos de ti para garantizar que tus datos personales son completos, precisos y actuales. Por favor,
                    infórmanos rápidamente si hay cualquier cambio o inexactitud en tus datos personales poniéndote en
                    contacto con reservaciones@villadivinapv.com . Gestionaremos tu petición de acuerdo con la
                    legislación aplicable.</p>
                <h2>Preguntas o reclamaciones</h2>
                <p>Si tienes alguna pregunta o duda sobre el procesamiento de tus datos personales por nuestra parte, o
                    quieres ejercer cualquiera de los derechos que tienes conforme a esta notificación, ponte en contacto
                    con nosotros mediante reservaciones@villadivinapv.com . También puedes ponerte en contacto con la
                    autoridad local en protección de datos con cualquier pregunta o reclamación.</p>
                <h2>Cambios en el aviso</h2>
                <p>De la misma forma que nuestro negocio cambia constantemente, esta Política de privacidad también puede
                    cambiar cada cierto tiempo. Si quieres ver los cambios realizados en esta Política de privacidad cada
                    cierto tiempo, te invitamos a acceder a esta Política de privacidad para ver los cambios. Si hacemos
                    cambios materiales o cambios que tengan alguna influencia en ti (por ejemplo, si empezamos a procesar
                    tus datos personales con otros fines distintos a los que se establecen aquí arriba), nos pondremos en
                    contacto contigo antes de empezar ese procesamiento.</p>
            </div>
        </div>
    </div>
</section>

<!-- Subscribe Section-->
<section class="section-small bg-img4" id="subscribe">
    <div class="overlay"></div>
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                <h3>Subscribe</h3>
                <h5>SIGN-UP TO RECEIVE FUTURE PROMOTIONS</h5>
                <!-- MailChimp Signup Form - Replace the form action in the line below with your MailChimp embed action! For more information on how to do this please visit the Docs!-->
                <form class="form-inline subscribe-form dark-form" id="mc-embedded-subscribe-form"
                      action="http://forbetterweb.us11.list-manage.com/subscribe/post?u=4f751a6c58b225179404715f0&amp;id=18fc72763a"
                      method="post" name="mc-embedded-subscribe-form" target="_blank" novalidate="">
                    <div class="input-group input-group-lg">
                        <input class="form-control" id="mce-EMAIL" type="email" name="EMAIL"
                               placeholder="Email address..."><span class="input-group-btn">
    <button class="btn btn-dark" id="mc-embedded-subscribe" type="submit" name="subscribe">Subscribe</button></span>
                        <div id="mce-responses"></div>
                        <div class="response" id="mce-error-response" style="display:none;"></div>
                        <div class="response" id="mce-success-response" style="display:none;"></div>
                    </div>
                </form>
                <!-- End MailChimp Signup Form--><img src="img/misc/mailchimp.png" alt="">
            </div>
        </div>
    </div>
</section>

<!-- Footer Section-->
<?php include('includes/frontend/modules/footer.php')?>
<!-- jQuery-->
<script src="js/jquery-1.12.4.min.js"></script>
<!-- Bootstrap Core JavaScript-->
<script src="js/bootstrap.min.js"></script>
<!-- Plugin JavaScript-->
<script src="js/jquery.easing.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/device.min.js"></script>
<script src="js/form.min.js"></script>
<script src="js/jquery.placeholder.min.js"></script>
<script src="js/jquery.shuffle.min.js"></script>
<script src="js/jquery.parallax.min.js"></script>
<script src="js/jquery.circle-progress.min.js"></script>
<script src="js/jquery.swipebox.min.js"></script>
<script src="js/smoothscroll.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/jquery.smartmenus.js"></script>
<!-- Custom Theme JavaScript-->
<script src="js/main.js"></script>
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</body>
</html>